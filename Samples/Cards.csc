using OpenTK;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using GCodeGeneratorNet.Misc;
using GCodeGeneratorNet.Core;
using GCodeGeneratorNet.Core.Geometry;
using GCodeGeneratorNet.Core.GCodes;
using GCodeGeneratorNet.Core.Misc;
using Xceed.Wpf.Toolkit.PropertyGrid.Attributes;
using System.Drawing;
using System.Drawing.Drawing2D;

public class StarParameters: IGCodeGeneratorParameters
{
	[DisplayName("General")]
	[ExpandableObject]
	public GCodeGeneratorHelperParameters HelperParams { get; set; }

    
    [Category("Font")]
    [DisplayName("Family")]
    [Editor(typeof(FontFamilyEditor), typeof(FontFamilyEditor))]
    public string FontFamily { get; set; }
    
    [Category("Font")]
    [DisplayName("Flatness")]
    public float FontFlatness { get; set;}
    
    [Category("Font")]
    [DisplayName("Size")]
    public float FontSize{ get; set;}
    
	public float CardWidth { get; set; }
	public float CardHeight { get; set; }
	public float CornerRadius { get; set;}
	public float Margin { get; set;}
	public int CardsX { get; set;}
	public int CardsY { get; set;}
	public int Page { get; set;}
    public float TextRpm { get; set; }
	
	[Editor(typeof(FileNameEditor), typeof(FileNameEditor))]
    public string File { get; set; }
    
    public StarParameters()
    {
    	FontFamily = "Corbel";
    	FontFlatness = 0.1f;
    	FontSize = 5;
    	CardWidth = 52;
    	CardHeight = 74;
    	CornerRadius = 4;
    	CardsX = 4;
    	CardsY = 4;
    	Margin = 4;
    	File = "cards_answers.tsv";
    	HelperParams = new GCodeGeneratorHelperParameters();
    	HelperParams.HorizontalFeedRate = 500;
        HelperParams.ToolRadius = 0.0f;
        HelperParams.SafetyHeight = 2;
        HelperParams.VerticalStep = 1;
        HelperParams.MaterialHeight = 0;
        HelperParams.BridgeCount = 0;
        HelperParams.LaserMode = true;
        TextRpm = HelperParams.MaxRpm / 2;
    }
}

public class StarGenerator: IGCodeGenerator, IFileHelperProvider
{
	public IFileHelper FileHelper { get; set; }
	
	StarParameters parameters = new StarParameters();
	
    public IGCodeGeneratorParameters Parameters 
    { 
    	get
    	{
    		return parameters;
    	}
    }
    
    public IGCodeResult Result
    {
        get
        {
            var gcg = new GCodeGeneratorHelper();
            var lines = FileHelper.ReadLines(parameters.File).ToArray();
            gcg.Parameters = parameters.HelperParams;
            var parts = new List<Part25D>();
            
            var cardsPerPage = parameters.CardsX * parameters.CardsY;
            var cardIndex = parameters.Page * cardsPerPage;
            for(var x = 0; x < parameters.CardsX; x++)
            {
            	for(var y = 0; y < parameters.CardsY; y++)
            	{
            		var text = lines[cardIndex];
            		var pos = new Vector2(parameters.CardWidth * x,
            			parameters.CardHeight * y);
            		parts.Add(Card(lines[cardIndex]).Positive().Offset(pos));
            		if (cardIndex < lines.Length - 1)
            		{
            			cardIndex++;
            		}
            	}
            }
            parts.Add(Card(lines[0]).Positive());
            foreach (Part25D p in parts)
            {
                gcg.Part25D(p);
            }
            var size = new Vector2(parameters.CardWidth * parameters.CardsX,
            	parameters.CardHeight * parameters.CardsY);
            GDebug.Write("Bounds " + size.ToString());
            gcg.GoToSafetyHeight();
            return new GCodeResult(parts, gcg.Codes);
        }
    }

    public Part25D Card(string text)
    {
        var pb = new PartBuilder();
        pb.AddRoundRect(new Vector2(parameters.CardWidth, parameters.CardHeight),
        	parameters.CornerRadius);
        pb.CreateContour();
		
		float fontSize = parameters.FontSize;
    	FontFamily ff = new FontFamily(parameters.FontFamily);
    	float flatness = parameters.FontFlatness;
    	Matrix m = new Matrix();
    	m.Translate(parameters.Margin, parameters.Margin - parameters.CardHeight);
    	pb.AddText(text, ff, 1, fontSize, m, new RectangleF(0, 0, 
    		parameters.CardWidth - parameters.Margin * 2, 
    		parameters.CardHeight - parameters.Margin * 2), flatness, false,
    		parameters.TextRpm);
    	pb.CreateHole();

        return pb.CreatePart(parameters.HelperParams.MaterialHeight);
    }

    public StarGenerator()
    {
    }
}
    
public static IGCodeGenerator GetGenerator()
{
    return new StarGenerator();
}

