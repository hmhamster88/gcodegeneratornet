using OpenTK;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using GCodeGeneratorNet.Misc;
using GCodeGeneratorNet.Core;
using GCodeGeneratorNet.Core.Geometry;
using GCodeGeneratorNet.Core.GCodes;
using GCodeGeneratorNet.Core.Misc;
using Xceed.Wpf.Toolkit.PropertyGrid.Attributes;
using System.Drawing;
using System.Drawing.Drawing2D;

public class Parameters: IGCodeGeneratorParameters
{
	[DisplayName("General")]
	[ExpandableObject]
	public GCodeGeneratorHelperParameters HelperParams { get; set; }
	
    public string Text { get; set; }
	
	public Parameters()
	{
		Text = "0123456789+-.ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		HelperParams = new GCodeGeneratorHelperParameters();
		HelperParams.MaterialHeight = 0;
		HelperParams.LaserMode = true;
		HelperParams.ToolRadius = 0;
	}
}

public class Generator: IGCodeGenerator
{
	Parameters parameters = new Parameters();
	
    public IGCodeGeneratorParameters Parameters 
    { 
    	get
    	{
    		return parameters;
    	}
    }
    
    public IGCodeResult Result
    {
        get
        {
            var gcg = new GCodeGeneratorHelper();
            gcg.Parameters = parameters.HelperParams;
            var parts = new List<Part25D>();
            
            var pb = new PartBuilder();
    		Matrix m = new Matrix();
    		m.Scale(0.1f, 0.1f);
    		pb.AddSimpleText(parameters.Text, new Vector2(0, 0), 0, true);
    		parts.Add(pb.CreatePart(parameters.HelperParams.MaterialHeight).Positive());

            foreach (Part25D p in parts)
            {
                gcg.Part25D(p);
            }
            
            gcg.RapidMoveToZero();
            return new GCodeResult(parts, gcg.Codes);
        }
    }
}

public static IGCodeGenerator GetGenerator()
{
    return new Generator();
}

