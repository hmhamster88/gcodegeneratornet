using OpenTK;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using GCodeGeneratorNet.Misc;
using GCodeGeneratorNet.Core;
using GCodeGeneratorNet.Core.Geometry;
using GCodeGeneratorNet.Core.GCodes;
using GCodeGeneratorNet.Core.Misc;
using Xceed.Wpf.Toolkit.PropertyGrid.Attributes;
using System.Text.RegularExpressions;

public class Parameters: IGCodeGeneratorParameters
{
	[DisplayName("General")]
	[ExpandableObject]
	public GCodeGeneratorHelperParameters HelperParams { get; set; }
	
	[Editor(typeof(FileNameEditor), typeof(FileNameEditor))]
    public string File { get; set; }
    
    [DisplayName("Mountain Line Rpm")]
    public float MountainLineRpm { get; set; }
	
	public Parameters()
	{
		File = @"dice.eps";
		HelperParams = new GCodeGeneratorHelperParameters();
		HelperParams.ToolRadius = 0;
		HelperParams.MaterialHeight = 0;
		HelperParams.LaserMode = true;
		MountainLineRpm = HelperParams.MaxRpm / 2;
	}
}

public class Generator: IGCodeGenerator, IFileHelperProvider
{
	Parameters parameters = new Parameters();
	
	public IFileHelper FileHelper { get; set; }
	
    public IGCodeGeneratorParameters Parameters 
    { 
    	get
    	{
    		return parameters;
    	}
    }
    
    public IGCodeResult Result
    {
        get
        {
            var gcg = new GCodeGeneratorHelper();
            gcg.Parameters = parameters.HelperParams;
            var parts = new List<Part25D>();
            
            var pb = new PartBuilder();
    		 using (var fs = FileHelper.OpenRead(parameters.File))
            {
                var ci = new System.Globalization.CultureInfo("en-us");
                var reader = new StreamReader(fs);
                string line;
                float? rpm = null;
                while ((line = reader.ReadLine()) != null)
                {
                    var rx = new Regex(@"(\d+.\d+)\s+(\d+.\d+) (lineto|moveto)");
                    Match m = rx.Match(line);
                    if (m.Success)
                    {
                        var point = new Vector2(float.Parse(m.Groups[1].Value, ci), float.Parse(m.Groups[2].Value, ci));
                        if (m.Groups[3].Value == "moveto")
                        {
                            var hole = pb.CreateHole(false);
                            if (hole != null)
                            {
                                hole.RpmOverride = rpm;
                            }
                        }
                        pb.AddPoint(point);
                    }
                    else if (line == "closepath")
                    {
                        var hole = pb.CreateHole(true);
                        if (hole != null)
                        {
                            hole.RpmOverride = rpm;
                        }
                    }
                    else if (line == "stroke")
                    {
                        var hole = pb.CreateHole(false);
                        if (hole != null)
                        {
                            hole.RpmOverride = rpm;
                        }
                    }
                    else if(line.Contains("setdash"))
                    {
                        if (line.StartsWith("[]"))
                        {
                            rpm = null;
                        }
                        else
                        {
                            rpm = parameters.MountainLineRpm;
                        }
                    }
                    else if(line.Contains("fill"))
                    {
                        pb.ClearHoles();
                    }
                }
            }
    		parts.Add(pb.CreatePart(parameters.HelperParams.MaterialHeight).Positive());
            // Add parts here
            foreach (Part25D p in parts)
            {
                gcg.Part25D(p);
            }
            return new GCodeResult(parts, gcg.Codes);
        }
    }
}

public static IGCodeGenerator GetGenerator()
{
    return new Generator();
}

