using OpenTK;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using GCodeGeneratorNet.Misc;
using GCodeGeneratorNet.Core;
using GCodeGeneratorNet.Core.Geometry;
using GCodeGeneratorNet.Core.GCodes;
using GCodeGeneratorNet.Core.Misc;
using Xceed.Wpf.Toolkit.PropertyGrid.Attributes;

public class Parameters: IGCodeGeneratorParameters
{
	[DisplayName("General")]
	[ExpandableObject]
	public GCodeGeneratorHelperParameters HelperParams { get; set; }
	
	[Editor(typeof(FileNameEditor), typeof(FileNameEditor))]
    public string File { get; set; }
	
	public Parameters()
	{
		HelperParams = new GCodeGeneratorHelperParameters();
		HelperParams.MaterialHeight = 0;
		HelperParams.LaserMode = true;
	}
}

public class Generator: IGCodeGenerator, IFileHelperProvider
{
	Parameters parameters = new Parameters();
	
    public IGCodeGeneratorParameters Parameters 
    { 
    	get
    	{
    		return parameters;
    	}
    }

    public IFileHelper FileHelper { get; set; }
    
    public IGCodeResult Result
    {
        get
        {
            var gcg = new GCodeGeneratorHelper();
            gcg.Parameters = parameters.HelperParams;
            var parts = new List<Part25D>();
            
            var pb = new PartBuilder();

    		parts.Add(pb.CreatePart(parameters.HelperParams.MaterialHeight).Positive());
            // Add parts here
            foreach (Part25D p in parts)
            {
                gcg.Part25D(p);
            }
            return new GCodeResult(parts, gcg.Codes);
        }
    }
}

public static IGCodeGenerator GetGenerator()
{
    return new Generator();
}

