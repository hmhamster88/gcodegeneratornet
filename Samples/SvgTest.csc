using OpenTK;
using System;
using System.IO;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using GCodeGeneratorNet.Misc;
using GCodeGeneratorNet.Core;
using GCodeGeneratorNet.Core.Geometry;
using GCodeGeneratorNet.Core.GCodes;
using GCodeGeneratorNet.Core.Misc;
using Xceed.Wpf.Toolkit.PropertyGrid.Attributes;
using Svg;

public class Parameters: IGCodeGeneratorParameters
{
	[DisplayName("General")]
	[ExpandableObject]
	public GCodeGeneratorHelperParameters HelperParams { get; set; }
	
	[Editor(typeof(FileNameEditor), typeof(FileNameEditor))]
    public string File { get; set; }
    
    [DisplayName("Mountain Line Rpm")]
    public float MountainLineRpm { get; set; }
	
	public Parameters()
	{
		File = @"unlit-bomb.svg";
		HelperParams = new GCodeGeneratorHelperParameters();
		HelperParams.MaterialHeight = 0;
		HelperParams.LaserMode = true;
		HelperParams.ToolRadius = 0;
		MountainLineRpm = HelperParams.MaxRpm / 2;
	}
}

public class Generator: IGCodeGenerator, IFileHelperProvider
{
	Parameters parameters = new Parameters();
	
	public IFileHelper FileHelper { get; set; }
	
    public IGCodeGeneratorParameters Parameters 
    { 
    	get
    	{
    		return parameters;
    	}
    }
    
    public IGCodeResult Result
    {
        get
        {
            var gcg = new GCodeGeneratorHelper();
            gcg.Parameters = parameters.HelperParams;
            var parts = new List<Part25D>();
            
            var pb = new PartBuilder();
            var svgDoc = Svg.SvgDocument.Open<Svg.SvgDocument>(FileHelper.OpenRead(parameters.File));
            pb.AddPath(svgDoc.Path, new System.Drawing.Drawing2D.Matrix(), 0.01f).ToArray();
            
    		parts.Add(pb.CreatePart(parameters.HelperParams.MaterialHeight).Positive());
            // Add parts here
            foreach (Part25D p in parts)
            {
                gcg.Part25D(p);
            }
            return new GCodeResult(parts, gcg.Codes);
        }
    }
}

public static IGCodeGenerator GetGenerator()
{
    return new Generator();
}

