using OpenTK;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using GCodeGeneratorNet.Misc;
using GCodeGeneratorNet.Core;
using GCodeGeneratorNet.Core.Geometry;
using GCodeGeneratorNet.Core.GCodes;
using GCodeGeneratorNet.Core.Misc;
using Xceed.Wpf.Toolkit.PropertyGrid.Attributes;
using System.Drawing;

public class Parameters: IGCodeGeneratorParameters
{
	[DisplayName("General")]
	[ExpandableObject]
	public GCodeGeneratorHelperParameters HelperParams { get; set; }
	
	[Editor(typeof(FileNameEditor), typeof(FileNameEditor))]
    public string File { get; set; }
    
    public int Threshold { get; set; }
    
    public bool Inverse	{ get; set; }
    
    public float Scale { get; set; }
    
    public float YStep { get; set; }
	
	public Parameters()
	{
		Threshold = 127;
		Inverse = false;
		Scale = 1;
		YStep = 1;
		File = @"Lenna.png";
		HelperParams = new GCodeGeneratorHelperParameters();
		HelperParams.MaterialHeight = 0;
		HelperParams.LaserMode = true;
		HelperParams.HorizontalFeedRate = 1000;
	}
}

public static int grayscale(Color color)
{
	return (color.R + color.G + color.B) / 3;
}

public class Generator: IGCodeGenerator, IFileHelperProvider
{
	Parameters parameters = new Parameters();
	
	public IFileHelper FileHelper { get; set; }
	
    public IGCodeGeneratorParameters Parameters 
    { 
    	get
    	{
    		return parameters;
    	}
    }
    
    public IGCodeResult Result
    {
        get
        {
            var gcg = new GCodeGeneratorHelper();
            gcg.Parameters = parameters.HelperParams;
            gcg.VerticalFeedTo(parameters.HelperParams.MaterialHeight);
            
            Bitmap bmp;
            using(var stream = FileHelper.OpenRead(parameters.File))
            {
            	bmp = new Bitmap(stream);
            }
            var threshold  = parameters.Threshold;
            var inverse = parameters.Inverse;
            bool on = false;
            int x = 0;
            var ystep = parameters.YStep;
            bool left = true;
            for(float y = 0; y < bmp.Height; y+= ystep)
            {
            	int iy = bmp.Height - (int)y - 1;
            	for(x = 0; x < bmp.Width; x++)
            	{
            		int rx = left?x:bmp.Width - x - 1;
            		
            		if(on && x == bmp.Width - 1)
            		{
            			gcg.SetSpindle(0);
            			gcg.HorizontalFeedTo(new Vector2(rx, y) * parameters.Scale);
            			on = false;
            			break;
            		}
            		
            		var color = bmp.GetPixel(rx, iy);
            		var gray = grayscale(color);
            		
            		bool turnOn;
            		if(inverse)
            		{
            			turnOn = gray > threshold;
            		}
            		else
            		{
            			turnOn = gray < threshold;
            		}
            		
            		if(turnOn)
            		{
            			if(!on)
            			{
            				gcg.SetSpindle(gcg.Parameters.MaxRpm);
            				gcg.RapidMoveTo(new Vector2(rx, y) * parameters.Scale);
            				on = true;
            			}
            		}
            		else
            		{
            			if(on)
            			{
            				gcg.SetSpindle(0);
            				gcg.HorizontalFeedTo(new Vector2(rx, y) * parameters.Scale);
            				on = false;
            			}
            		}
            	}
            	left = !left;
            }
            return new GCodeResult(new Part25D[]{}, gcg.Codes);
        }
    }
}

public static IGCodeGenerator GetGenerator()
{
    return new Generator();
}

