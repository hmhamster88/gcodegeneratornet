using OpenTK;
using System;
using System.IO;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using GCodeGeneratorNet.Misc;
using GCodeGeneratorNet.Core;
using GCodeGeneratorNet.Core.Geometry;
using GCodeGeneratorNet.Core.GCodes;
using GCodeGeneratorNet.Core.Misc;
using Xceed.Wpf.Toolkit.PropertyGrid.Attributes;
using IxMilia.Dxf;
using IxMilia.Dxf.Entities;

public class Parameters: IGCodeGeneratorParameters
{
	[DisplayName("General")]
	[ExpandableObject]
	public GCodeGeneratorHelperParameters HelperParams { get; set; }
	
	[Editor(typeof(FileNameEditor), typeof(FileNameEditor))]
    public string File { get; set; }
    
    [DisplayName("Mountain Line Rpm")]
    public float MountainLineRpm { get; set; }
	
	public Parameters()
	{
		File = @"dice.dxf";
		HelperParams = new GCodeGeneratorHelperParameters();
		HelperParams.MaterialHeight = 0;
		HelperParams.LaserMode = true;
		HelperParams.ToolRadius = 0;
		MountainLineRpm = HelperParams.MaxRpm / 2;
	}
}

public class Generator: IGCodeGenerator, IFileHelperProvider
{
	Parameters parameters = new Parameters();
	
	public IFileHelper FileHelper { get; set; }
	
    public IGCodeGeneratorParameters Parameters 
    { 
    	get
    	{
    		return parameters;
    	}
    }
    
    public IGCodeResult Result
    {
        get
        {
            var gcg = new GCodeGeneratorHelper();
            gcg.Parameters = parameters.HelperParams;
            var parts = new List<Part25D>();
            
            var pb = new PartBuilder();
            DxfFile dxfFile;
            using (var fs = FileHelper.OpenRead(parameters.File))
            {
                dxfFile = DxfFile.Load(fs);
            }

            var cutLines = new List<Tuple<Vector2, Vector2>>();
            var mountainLines = new List<Tuple<Vector2, Vector2>>();

            foreach (DxfEntity entity in dxfFile.Entities)
            {
                switch (entity.EntityType)
                {
                    case DxfEntityType.Line:
                        DxfLine line = (DxfLine)entity;
                        var tuple = new Tuple<Vector2, Vector2>(new Vector2((float)line.P1.X, (float)line.P1.Y), new Vector2((float)line.P2.X, (float)line.P2.Y));
                        switch (line.Layer)
                        {
                            case "MountainLine":
                                mountainLines.Add(tuple);
                                break;
                            case "CutLine":
                                cutLines.Add(tuple);
                                break;
                        }
                        break;
                }
            }

            foreach (var line in mountainLines)
            {
                pb.AddPoint(line.Item1);
                pb.AddPoint(line.Item2);
                pb.CreateHole(false).RpmOverride = parameters.MountainLineRpm;
            }

            foreach (var line in cutLines)
            {
                pb.AddPoint(line.Item1);
                pb.AddPoint(line.Item2);
                pb.CreateHole(false);
            }
    		parts.Add(pb.CreatePart(parameters.HelperParams.MaterialHeight).Positive());
            // Add parts here
            foreach (Part25D p in parts)
            {
                gcg.Part25D(p);
            }
            return new GCodeResult(parts, gcg.Codes);
        }
    }
}

public static IGCodeGenerator GetGenerator()
{
    return new Generator();
}

