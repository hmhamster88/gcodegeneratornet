using OpenTK;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using GCodeGeneratorNet.Misc;
using GCodeGeneratorNet.Core;
using GCodeGeneratorNet.Core.Geometry;
using GCodeGeneratorNet.Core.GCodes;
using GCodeGeneratorNet.Core.Misc;
using Xceed.Wpf.Toolkit.PropertyGrid.Attributes;
using System.Drawing;
using System.Drawing.Drawing2D;

public class StarParameters: IGCodeGeneratorParameters
{
	[DisplayName("General")]
	[ExpandableObject]
	public GCodeGeneratorHelperParameters HelperParams { get; set; }
    [DisplayName("Radius")]
    public float radius { get; set; }
    [DisplayName("Arrow Height")]
    public float arrowHeight { get; set; }
    [DisplayName("Arrow Width")]
    public float arrowWidth { get; set; }
    [DisplayName("Arrow Base Width")]
    public float arrowBaseWidth { get; set; }
    [DisplayName("Circle Radius")]
    public float r1 { get; set; }
    [DisplayName("Horizontall Fill")]
    public float horizontalFill { get; set;}
    
    [Category("Font")]
    [DisplayName("Family")]
    [Editor(typeof(FontFamilyEditor), typeof(FontFamilyEditor))]
    public string FontFamily { get; set; }
    
    [Category("Font")]
    [DisplayName("Flatness")]
    public float FontFlatness { get; set;}
    
    [Category("Font")]
    [DisplayName("Size")]
    public float FontSize{ get; set;}
    
    [Category("Bounding Box")]
    [DisplayName("Enabled")]
    public bool BoundingBoxEnabled { get; set; }
    
    [Category("Bounding Box")]
    [DisplayName("Inflate")]
    public float BoundingBoxInflate { get; set; }
    
    public StarParameters()
    {
    	radius = 20;
    	arrowHeight = 0.3f;
    	arrowWidth = 0.15f;
    	arrowBaseWidth = 0.08f;
    	r1 = 0.59f;
    	horizontalFill = 0;
    	FontFamily = "Corbel";
    	FontFlatness = 0.01f;
    	FontSize = 1;
    	HelperParams = new GCodeGeneratorHelperParameters();
    	HelperParams.HorizontalFeedRate = 500;
        HelperParams.ToolRadius = 0.0f;
        HelperParams.SafetyHeight = 2;
        HelperParams.VerticalStep = 1;
        HelperParams.MaterialHeight = 0;
        HelperParams.BridgeCount = 0;
        HelperParams.LaserMode = true;
    }
}

public class StarGenerator: IGCodeGenerator
{
	StarParameters parameters = new StarParameters();
	
    public IGCodeGeneratorParameters Parameters 
    { 
    	get
    	{
    		return parameters;
    	}
    }
    
    public IGCodeResult Result
    {
        get
        {
            var gcg = new GCodeGeneratorHelper();
            gcg.Parameters = parameters.HelperParams;
            var parts = new List<Part25D>();
            parts.Add(ChaosStar());
            foreach (Part25D p in parts)
            {
                gcg.Part25D(p);
            }
            gcg.GoToSafetyHeight();
            return new GCodeResult(parts, gcg.Codes);
        }
    }

    public Part25D ChaosStar()
    {
        var pb = new PartBuilder();
        var a1 = Math.Asin(parameters.arrowBaseWidth / parameters.r1);
        var r2 = parameters.r1 - parameters.arrowBaseWidth * 2;
        foreach (Angle angle in Angle.Angles(8))
        {
            var mainVector = angle.HorizontalVector * parameters.radius;
            var mp = mainVector.PerpendicularRight;
            var arrowBase = mainVector * (1 - parameters.arrowHeight);
            pb.AddArc(new Vector2(), parameters.radius * parameters.r1,
                angle - Math.PI / 4 + a1, angle - a1, RotateDirection.CCW);
            pb.AddPoint(arrowBase + (mp * parameters.arrowBaseWidth));
            pb.AddPoint(arrowBase + mp * parameters.arrowWidth);
            pb.AddPoint(mainVector);
            pb.AddPoint(arrowBase - mp * parameters.arrowWidth);
            pb.AddPoint(arrowBase - (mp * parameters.arrowBaseWidth));

        }
        pb.CreateContour();
        float r3 = parameters.arrowBaseWidth / (float)Math.Sin(Math.PI / 8);
		pb.AddArc(new Vector2(), r2 * parameters.radius, Angle.Left, Angle.Left, RotateDirection.CCW);
		pb.CreateHole();
		
		float fontSize = parameters.radius * parameters.FontSize;
    	FontFamily ff = new FontFamily(parameters.FontFamily);
    	float flatness = parameters.FontFlatness;
    	Matrix m = new Matrix();
    	m.Translate(-(parameters.radius + fontSize/1.5f), 0);
    	pb.AddText("DR", ff, 1, fontSize, m, flatness);
    
    	m = new Matrix();
    	m.Translate((parameters.radius + fontSize/1.5f), 0);
    	pb.AddText("ZD", ff, 1, fontSize, m, flatness);
    
    	if(parameters.BoundingBoxEnabled)
    	{
    		pb.AddRect(pb.BoundingBox.Inflated(parameters.BoundingBoxInflate));
    		pb.CreateHole();
    	}

        pb.AddHorizontalFill(pb.AllContours, parameters.horizontalFill);
        return pb.CreatePart(parameters.HelperParams.MaterialHeight);
    }

    public StarGenerator()
    {
    }
}
    
public static IGCodeGenerator GetGenerator()
{
    return new StarGenerator();
}

