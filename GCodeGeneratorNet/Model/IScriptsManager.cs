﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCodeGeneratorNet.Model
{
    public interface IScriptsManager
    {
        GCodeScript Script { get; set; }
        void Save(string filePath);
        void Open(string filePath);
        void New();
        void ExportGCode(string filePath);
        void ExportSTL(string filePath);
    }
}
