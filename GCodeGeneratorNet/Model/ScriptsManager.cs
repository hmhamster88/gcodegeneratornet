﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GCodeGeneratorNet.Core;
using GCodeGeneratorNet.Core.Geometry;
using System.Xml;
using System.Xml.Serialization;

namespace GCodeGeneratorNet.Model
{
    public class ScriptsManager : IScriptsManager, INotifyPropertyChanged, IGCodeResultProvider
    {
        public event PropertyChangedEventHandler PropertyChanged;

        void notifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        GCodeScript script = new GCodeScript();
        public GCodeScript Script
        {
            get
            {
                return script;
            }

            set
            {
                script = value;
                notifyPropertyChanged(nameof(Script));
            }
        }

        public IGCodeResult Result
        {
            get
            {
                return Script?.Result;
            }
        }

        public void New()
        {
            Script = new GCodeScript();
        }

        public void Open(string filePath)
        {
            Script = new GCodeScript();
            Script.Text = File.ReadAllText(filePath);
            Properties.Settings.Default.LastFile = Script.FilePath = filePath;
            Script.LoadParameters();
        }

        public void Save(string filePath)
        {
            File.WriteAllText(filePath, Script.Text);
            Properties.Settings.Default.LastFile = Script.FilePath = filePath;
            Script.SaveParameters();
        }

        public ScriptsManager()
        {

        }

        public void ExportGCode(string filePath)
        {
            if (Script.Result != null)
            {
                if (File.Exists(filePath))
                {
                    File.Delete(filePath);
                }
                using (var file = File.OpenWrite(filePath))
                {
                    var sr = new StreamWriter(file);
                    foreach (var code in Script.Result.Codes)
                    {
                        sr.WriteLine(code.ToString());
                    }
                    sr.Flush();
                }
            }
        }

        public void ExportSTL(string filePath)
        {
            var result = Script.Result;
            if (result != null)
            {
                using (var f = File.OpenWrite(filePath))
                {
                    var trangles = new List<Triangle3D>();

                    foreach (var part in result.Parts)
                    {
                        trangles.AddRange(part.ToTrangles());
                    }
                    STLWriter.WriteSTL(f, trangles);
                }
            }
        }
    }
}
