﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace GCodeGeneratorNet.Model
{
    public class View3DSettings : IView3DSettings, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        void notifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public bool DrawParts
        {
            get
            {
                return Properties.Settings.Default.DrawParts;
            }

            set
            {
                Properties.Settings.Default.DrawParts = value;
                notifyPropertyChanged(nameof(DrawParts));
            }
        }

        public bool DrawPartsEdges
        {
            get
            {
                return Properties.Settings.Default.DrawPartsEdges;
            }

            set
            {
                Properties.Settings.Default.DrawPartsEdges = value;
                notifyPropertyChanged(nameof(DrawPartsEdges));
            }
        }

        public bool DrawPaths
        {
            get
            {
                return Properties.Settings.Default.DrawPaths;
            }

            set
            {
                Properties.Settings.Default.DrawPaths = value;
                notifyPropertyChanged(nameof(DrawPaths));
            }
        }

        public Color PartsColor
        {
            get
            {
                return Properties.Settings.Default.PartsColor;
            }

            set
            {
                Properties.Settings.Default.PartsColor = value;
                notifyPropertyChanged(nameof(PartsColor));
            }
        }

        public Color PartsEdgesColor
        {
            get
            {
                return Properties.Settings.Default.PartsEdgesColor;
            }

            set
            {
                Properties.Settings.Default.PartsEdgesColor = value;
                notifyPropertyChanged(nameof(PartsEdgesColor));
            }
        }

        public Color BackgroundColor
        {
            get
            {
                return Properties.Settings.Default.BackgroundColor;
            }

            set
            {
                Properties.Settings.Default.BackgroundColor = value;
                notifyPropertyChanged(nameof(BackgroundColor));
            }
        }
    }
}
