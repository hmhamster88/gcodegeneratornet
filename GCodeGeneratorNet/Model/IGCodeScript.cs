﻿using GCodeGeneratorNet.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCodeGeneratorNet.Model
{
    public interface IGCodeScript
    {
        string Text { get; set; }
        string FilePath { get; set; }
        IGCodeGenerator Generator { get; set; }
        IGCodeResult Result { get; set; }
    }
}
