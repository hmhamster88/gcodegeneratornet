﻿using GCodeGeneratorNet.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCodeGeneratorNet.Model
{
    public interface IGCodeGenerationManger
    {
        bool Generating { get; }

        IGCodeResult Generate(IGCodeGenerator generator);
    }
}
