﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GCodeGeneratorNet.Core;
using System.ComponentModel;

namespace GCodeGeneratorNet.Model
{
    public class GCodeGenerationManger : IGCodeGenerationManger, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        void notifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        bool generating;
    
        public bool Generating
        {
            get
            {
                return generating;
            }
            private set
            {
                generating = value;
                notifyPropertyChanged(nameof(Generating));
            }
        }

        public IGCodeResult Generate(IGCodeGenerator generator)
        {
            IGCodeResult result = null;
            Generating = true;
            if (generator != null)
            {
                result = generator.Result;
            }
            Generating = false;
            return result;
        }
    }
}
