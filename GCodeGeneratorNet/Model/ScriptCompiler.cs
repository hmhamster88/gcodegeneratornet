﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GCodeGeneratorNet.Core;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.CodeDom.Compiler;
using System.Windows.Data;
using csscript;
using CSScriptLibrary;

namespace GCodeGeneratorNet.Model
{
    public class ScriptCompiler : IScriptComplier, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        void notifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        bool compiling;

        public bool Compiling
        {
            get
            {
                return compiling;
            }
            set
            {
                compiling = value;
                notifyPropertyChanged(nameof(Compiling));
            }
        }

        ObservableCollection<CompilerError> errors = new ObservableCollection<CompilerError>();
        public ObservableCollection<CompilerError> Errors
        {
            get
            {
                return errors;
            }
        }

        public IGCodeGenerator Compile(IGCodeScript script)
        {
            var source = script.Text;
            IGCodeGenerator result = null;
            Errors.Clear();
            Compiling = true;
            try
            {
                if (source != null)
                {

                    var asmhelper = new AsmHelper(CSScript.LoadMethod(source));
                    result = asmhelper.Invoke("*.GetGenerator") as IGCodeGenerator;
                    var filehelperProvider = result as IFileHelperProvider;
                    if (filehelperProvider != null)
                    {
                        filehelperProvider.FileHelper = new FileHelper(script.FilePath);
                    }
                }
            }
            catch (CompilerException ex)
            {
                CompilerErrorCollection errs = (CompilerErrorCollection)ex.Data["Errors"];
                foreach (CompilerError err in errs)
                {
                    Errors.Add(err);
                }
            }
            catch(Exception ex)
            {
                Errors.Add(new CompilerError("", 0, 0, "", ex.Message));
            }
            Compiling = false;
            return result;
        }

        private readonly object errorsnCollectionLock = new object();
        public ScriptCompiler()
        {
            BindingOperations.EnableCollectionSynchronization(errors, errorsnCollectionLock);
        }
    }
}
