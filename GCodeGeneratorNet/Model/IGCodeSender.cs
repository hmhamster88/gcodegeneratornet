﻿using GCodeGeneratorNet.Core.GCodes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCodeGeneratorNet.Model
{
    public interface IGCodeSender
    {
        bool IsConnected { get; }
        string SelectedPort { get; set; }
        IEnumerable<string> Ports { get; }
        void RefreshPorts();
        void SendAllCodes();
        void SendCommand(string command);
        void Connect();
        void Disconnect();
        void SendCodes(IEnumerable<IGCode> codes);
        void Reset();
    }
}
