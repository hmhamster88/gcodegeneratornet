﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCodeGeneratorNet.Model
{
    public interface IErrorReporter
    {
        void Report(Exception ex);
    }
}
