﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace GCodeGeneratorNet.Model
{
    public interface IView3DSettings
    {
        bool DrawPaths { get; set; }
        bool DrawParts { get; set; }
        bool DrawPartsEdges { get; set; }
        Color PartsColor { get; set; }
        Color PartsEdgesColor { get; set; }
        Color BackgroundColor { get; set; }
    }
}
