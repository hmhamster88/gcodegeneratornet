﻿using GCodeGeneratorNet.Core;
using GCodeGeneratorNet.Misc;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace GCodeGeneratorNet.Model
{
    public class GCodeScript : IGCodeScript, INotifyPropertyChanged
    {
        const string configExt = ".config";

        public event PropertyChangedEventHandler PropertyChanged;

        void notifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private string text;
        public string Text
        {
            get
            {
                return text;
            }
            set
            {
                text = value;
                notifyPropertyChanged(nameof(Text));
            }
        }

        private string filepath;

        public string FilePath
        {
            get
            {
                return filepath;
            }
            set
            {
                filepath = value;
                notifyPropertyChanged(nameof(FilePath));
            }
        }

        public void SaveParameters()
        {
            if(filepath != null && Generator != null && Generator.Parameters != null)
            {
                var parameters = Generator.Parameters;
                if (parameters != null)
                {
                    var serializer = new XmlSerializer(parameters.GetType());
                    using (var config = File.OpenWrite(FilePath + configExt))
                    {
                        serializer.Serialize(config, parameters);
                    }
                }
            }
        }

        public void LoadParameters()
        {
            if (filepath != null && Generator != null && Generator.Parameters != null)
            {
                try
                {
                    var parameters = Generator.Parameters;
                    var serializer = new XmlSerializer(parameters.GetType());
                    using (var config = File.OpenRead(FilePath + configExt))
                    {
                        lastParameters = serializer.Deserialize(config) as IGCodeGeneratorParameters;
                    }
                }
                catch { }
            }
        }

        public void ResetParameters()
        {
            if(generator != null && generator.Parameters != null)
            {
                lastParameters = Activator.CreateInstance(generator.Parameters.GetType()) as IGCodeGeneratorParameters;
                PropertyCopy.CopyProperties(lastParameters, generator.Parameters);
                notifyPropertyChanged(nameof(Generator));
            }
        }

        IGCodeGeneratorParameters lastParameters;

        IGCodeGenerator generator;
        public IGCodeGenerator Generator
        {
            get
            {
                return generator;
            }

            set
            {
                generator = value;
                if(lastParameters == null)
                {
                    LoadParameters();
                }
                
                if(generator != null)
                {
                    if(lastParameters != null && generator.Parameters != null)
                    {
                        PropertyCopy.CopyProperties(lastParameters, generator.Parameters);
                    }
                    lastParameters = generator.Parameters;
                }
                notifyPropertyChanged(nameof(Generator));
            }
        }

        public IGCodeResult Result
        {
            get
            {
                return result;
            }

            set
            {
                result = value;
                notifyPropertyChanged(nameof(Result));
            }
        }

        IGCodeResult result;
    }
}
