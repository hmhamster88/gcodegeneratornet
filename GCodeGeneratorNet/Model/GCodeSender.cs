﻿using GCodeGeneratorNet.Core.GCodes;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using Xceed.Wpf.Toolkit.PropertyGrid.Attributes;

namespace GCodeGeneratorNet.Model
{
    public class GCodeSender : INotifyPropertyChanged, IGCodeSender
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string popertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(popertyName));
        }
        ObservableCollection<string> ports = new ObservableCollection<string>();
        public IEnumerable<string> Ports
        {
            get
            {
                return ports;
            }
        }

        bool isConnected = false;

        [ExpandableObject]
        public bool IsConnected
        {
            get
            {
                return isConnected;
            }

            private set
            {
                isConnected = value;
                NotifyPropertyChanged(nameof(IsConnected));
            }
        }

        string selectedPort;

        public string SelectedPort
        {
            get
            {
                return selectedPort;
            }

            set
            {
                selectedPort = value;
                NotifyPropertyChanged(nameof(SelectedPort));
            }
        }

        public void RefreshPorts()
        {
            var lastSelectedPort = SelectedPort;
            ports.Clear();
            foreach(var port in SerialPort.GetPortNames())
            {
                ports.Add(port);
            }
            if(!Ports.Contains(lastSelectedPort))
            {
                SelectedPort = Ports.FirstOrDefault();
            }
            else
            {
                SelectedPort = lastSelectedPort;
            }
        }

        object outputCollectionLock = new object();
        public ObservableCollection<string> Output { get; private set; }

        void AddOutput(string text)
        {
            Output.Add(text);
            while(Output.Count > 100)
            {
                Output.RemoveAt(0);
            }
        }

        SerialPort port;

        public void Connect()
        {
            try
            {
                port = new SerialPort(SelectedPort, 115200, Parity.None, 8, StopBits.One);
                port.DtrEnable = false;
                port.NewLine = "\n";
                port.Open();
                port.ReadTimeout = -1;
                port.Write("\n");
                
                connectionStream = port.BaseStream;
                IsConnected = true;
                Task.Factory.StartNew(() =>
                {
                    var reader = new StreamReader(connectionStream, ASCIIEncoding.ASCII);
                    while (IsConnected)
                    {
                        var line = reader.ReadLine();
                        AddOutput(line);
                        if (sendedCodes.Count > 0)
                        {
                            sendedCodes.Take();
                        }
                    }
                });
                
            }
            catch (Exception ex)
            {
                AddOutput(ex.Message);
            }
        }

        public void Disconnect()
        {
            port.Close();
            IsConnected = false;
        }

        Stream connectionStream;

        [Dependency]
        public IGCodeResultProvider ResultProvider { get; set; }

        public GCodeSender()
        {
            Output = new ObservableCollection<string>();
            BindingOperations.EnableCollectionSynchronization(Output, outputCollectionLock);
            RefreshPorts();
        }

        BlockingCollection<IGCode> sendedCodes = new BlockingCollection<IGCode>(1);

        public void SendAllCodes()
        {
            if(ResultProvider.Result != null)
            {
                SendCodes(ResultProvider.Result.Codes);
            }
        }

        public void SendCodes(IEnumerable<IGCode> codes)
        {
            if (IsConnected)
            {
                var array = codes.ToArray();
                Task.Factory.StartNew(() =>
                {
                    var writer = new StreamWriter(connectionStream, ASCIIEncoding.ASCII);
                    foreach (var code in array)
                    {
                        sendedCodes.Add(code);
                        writer.WriteLine(code.ToString().Trim());
                        writer.Flush();
                        AddOutput(code.ToString().Trim());
                    }
                });
            }
        }

        public void SendCommand(string command)
        {
            if (IsConnected)
            {
                var writer = new StreamWriter(connectionStream, ASCIIEncoding.ASCII);
                writer.WriteLine(command);
                writer.Flush();
                AddOutput(command);
            }
        }

        public void Reset()
        {
            connectionStream.WriteByte(0x18);
        }
    }
}
