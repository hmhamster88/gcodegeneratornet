﻿using GCodeGeneratorNet.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCodeGeneratorNet.Model
{
    public interface IScriptComplier
    {
        IGCodeGenerator Compile(IGCodeScript script);
        bool Compiling { get; }
    }
}
