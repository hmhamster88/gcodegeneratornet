﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace GCodeGeneratorNet.Model
{
    public class ErrorReporter : IErrorReporter
    {
        public void Report(Exception ex)
        {
            App.Current.Dispatcher.Invoke(() =>
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            });
        }
    }
}
