﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Xceed.Wpf.Toolkit.PropertyGrid;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;

namespace GCodeGeneratorNet.Misc
{
    public class FontFamilyEditor : Xceed.Wpf.Toolkit.PropertyGrid.Editors.ITypeEditor
    {
        FrameworkElement ITypeEditor.ResolveEditor(PropertyItem propertyItem)
        {
            ComboBox comboBox = new ComboBox();

            comboBox.ItemsSource = FontFamily.Families.Select(f => f.Name);

            //create the binding from the bound property item to the editor
            var _binding = new Binding("Value"); //bind to the Value property of the PropertyItem
            _binding.Source = propertyItem;
            _binding.ValidatesOnExceptions = true;
            _binding.ValidatesOnDataErrors = true;
            _binding.Mode = propertyItem.IsReadOnly ? BindingMode.OneWay : BindingMode.TwoWay;
            BindingOperations.SetBinding(comboBox, ComboBox.SelectedItemProperty, _binding);
            return comboBox;
        }
    }
}
