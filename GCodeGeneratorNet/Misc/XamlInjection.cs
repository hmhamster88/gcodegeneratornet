﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Markup;

namespace GCodeGeneratorNet.Misc
{
    public class XamlInjection : MarkupExtension
    {
        public Type Type { get; set; }

        public IUnityContainer Container { get; set; }

        public XamlInjection()
        {

        }

        public XamlInjection(Type type)
        {
            Type = type;
        }

        public XamlInjection(Type type, IUnityContainer container)
        {
            Type = type;
            Container = container;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            if(Type == null || Container == null)
            {
                return null;
            }
            return Container.Resolve(Type);
        }
    }
}
