﻿using GCodeGeneratorNet.Model;
using GCodeGeneratorNet.ModelView;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCodeGeneratorNet.Misc
{
    public class InjectionContainer: UnityContainer
    {
        public InjectionContainer()
        {
            var scriptsManager = new ScriptsManager();
            var container = ((IUnityContainer)this);
            container.RegisterType<IMainWindowModelView, MainWindowModelView>()
                .RegisterInstance<IScriptsManager>(scriptsManager)
                .RegisterInstance<IGCodeResultProvider>(scriptsManager)
                .RegisterType<IScriptComplier, ScriptCompiler>()
                .RegisterType<IGCodeGenerationManger, GCodeGenerationManger>()
                .RegisterInstance<IGCodeSender>(container.BuildUp(new GCodeSender()))
                .RegisterInstance<IView3DSettings>(container.BuildUp(new View3DSettings()))
                .RegisterType<ISettingsModelView, SettingsModelView>()
                .RegisterType<IErrorReporter, ErrorReporter>();
        }
    }
}
