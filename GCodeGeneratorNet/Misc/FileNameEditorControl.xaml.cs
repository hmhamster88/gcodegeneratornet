﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Xceed.Wpf.Toolkit.PropertyGrid;

namespace GCodeGeneratorNet.Misc
{
    /// <summary>
    /// Логика взаимодействия для FileNameEditorControl.xaml
    /// </summary>
    public partial class FileNameEditorControl : UserControl
    {
        public FileNameEditorControl()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new OpenFileDialog();
            if (dialog.ShowDialog() == true)
            {
                var prop = DataContext as PropertyItem;
                if(prop != null)
                {
                    prop.Value = dialog.FileName;
                }
            }
        }
    }
}
