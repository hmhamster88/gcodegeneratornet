﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK.Graphics.OpenGL;
using System.Windows.Forms;
using System.Windows.Media;

namespace GCodeGeneratorNet.Graphics
{
    class PathViewControl : GLControl
    {
        bool loaded = false;
        Vector3 viewRotation = new Vector3(-135, -45, 0);
        Vector3 viewPan;
        float viewScale = 1;

        IEnumerable<Path3DColored> paths;

        Path3DColored highlightPath;

        System.Drawing.Point lastMousePoint;
        MouseButtons lastButton;

        public Path3DColored HighlightPath
        {
            get
            {
                return highlightPath;
            }

            set
            {
                highlightPath = value;
                Invalidate();
            }
        }

        public float ViewScale
        {
            get
            {
                return viewScale;
            }

            set
            {
                viewScale = value;
                updateProjection();
            }
        }

        bool drawParts = true;

        public bool DrawParts
        {
            get
            {
                return drawParts;
            }

            set
            {
                drawParts = value;
                Invalidate();
            }
        }

        bool drawPaths = true;
        public bool DrawPaths
        {
            get
            {
                return drawPaths;
            }

            set
            {
                drawPaths = value;
                Invalidate();
            }
        }

        bool drawPartsEdges = true;

        public bool DrawPartsEdges
        {
            get
            {
                return drawPartsEdges;
            }

            set
            {
                drawPartsEdges = value;
                Invalidate();
            }
        }

        Color partsColor = Color.FromArgb(127, 127, 127, 127);
        public Color PartsColor
        {
            get
            {
                return partsColor;
            }

            set
            {
                partsColor = value;
                Invalidate();
            }
        }

        public Color PartsEdgesColor
        {
            get
            {
                return partsEdgesColor;
            }

            set
            {
                partsEdgesColor = value;
                Invalidate();
            }
        }

        IEnumerable<Part3DModel> parts;
        public IEnumerable<Part3DModel> Parts
        {
            get
            {
                return parts;
            }

            set
            {
                if(parts != null)
                {
                    foreach(var part in parts)
                    {
                        part.Dispose();
                    }
                }
                parts = value;
                Invalidate();
            }
        }

        Color backgroundColor = Colors.White;

        public Color BackgroundColor
        {
            get
            {
                return backgroundColor;
            }

            set
            {
                backgroundColor = value;
                Invalidate();
            }
        }

        Color partsEdgesColor = Colors.Black;

        public PathViewControl()
        {
            MouseMove += PathViewControl_MouseMove;
            MouseWheel += PathViewControl_MouseWheel;
        }

        private void PathViewControl_MouseWheel(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Delta > 0)
            {
                ViewScale /= 2;
            }
            else
            {
                ViewScale *= 2;
            }
            Invalidate();
        }

        private void PathViewControl_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if(lastMousePoint != null && e.Button == lastButton)
            {
                System.Drawing.Point delta = new System.Drawing.Point(e.Location.X - lastMousePoint.X,
                    e.Location.Y - lastMousePoint.Y);
                if (e.Button == MouseButtons.Left)
                {
                    viewRotation.X += delta.X;
                    viewRotation.Y += delta.Y;
                }

                if (e.Button == MouseButtons.Middle)
                {
                    viewPan.X += delta.X;
                    viewPan.Y += delta.Y;
                }
            }
            lastMousePoint = e.Location;
            lastButton = e.Button;
            Invalidate();
        }

        public void LoadPoints(IEnumerable<Path3DColored> paths)
        {
            if (loaded)
            {
                if (paths != null)
                {
                    foreach (var path in paths)
                    {
                        path.Dispose();
                    }
                }
                this.paths = paths;
                Invalidate();
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            try
            {
                Version version = new Version(GL.GetString(StringName.Version).Substring(0, 3));
                Version target = new Version(1, 5);
                if (version < target)
                {
                    throw new NotSupportedException(String.Format(
                        "OpenGL {0} is required (you only have {1}).", target, version));
                }

                GL.ClearColor(System.Drawing.Color.MidnightBlue);
                GL.Enable(EnableCap.DepthTest);
                loaded = true;
            }
            catch
            {
            }
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            if (!loaded)
            {
                return;
            }
            GL.Viewport(0, 0, Width, Height);
            updateProjection();
        }

        private void updateProjection()
        {
            Matrix4 perpective = Matrix4.CreateOrthographic(Width * ViewScale, Height * ViewScale, -1000, 1000);
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadMatrix(ref perpective);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            if (!loaded)
            {
                return;
            }

            GL.Enable(EnableCap.DepthTest);
            var fc = colorToFloatArray(BackgroundColor);
            GL.ClearColor(fc[0], fc[1], fc[2], fc[3]);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            GL.Light(LightName.Light0, LightParameter.Position, new float[] { 10.0f, 10.0f, 10.0f, 0 });
            GL.Light(LightName.Light0, LightParameter.Ambient, new float[] { 0.3f, 0.3f, 0.3f, 1.0f });
            GL.Light(LightName.Light0, LightParameter.Diffuse, new float[] { 1.0f, 1.0f, 1.0f, 1.0f });
            GL.Light(LightName.Light0, LightParameter.Specular, new float[] { 1.0f, 1.0f, 1.0f, 1.0f });
            GL.Light(LightName.Light0, LightParameter.SpotExponent, new float[] { 1.0f, 1.0f, 1.0f, 1.0f });
            GL.LightModel(LightModelParameter.LightModelAmbient, new float[] { 0.2f, 0.2f, 0.2f, 1.0f });
            GL.LightModel(LightModelParameter.LightModelTwoSide, 1);
            GL.LightModel(LightModelParameter.LightModelLocalViewer, 1);

            Matrix4 lookat = Matrix4.LookAt(5, 5, 5, 0, 0, 0, 0, 0, 1);

            GL.MatrixMode(MatrixMode.Modelview);
            GL.LoadMatrix(ref lookat);

            GL.LoadIdentity();

            GL.Translate(viewPan.X * ViewScale, -viewPan.Y * ViewScale, 0);
            GL.Rotate(viewRotation.Y, Vector3d.UnitX);
            GL.Rotate(viewRotation.X, Vector3d.UnitZ);

            GL.Begin(BeginMode.Lines);
            GL.Color3(1.0f, 0, 0);

            var length = 1;

            GL.Vertex3(0, 0, 0);
            GL.Vertex3(length, 0, 0);

            GL.Color3(0.0f, 1.0f, 0);
            GL.Vertex3(0, 0, 0);
            GL.Vertex3(0, length, 0);

            GL.Color3(0.0f, 0.0f, 1.0f);
            GL.Vertex3(0, 0, 0);
            GL.Vertex3(0, 0, length);

            GL.End();
            if (DrawPaths)
            {
                drawPathsM();
            }
            drawPartsM();
            if (DrawPaths)
            {
                drawHightlightPath();
            }

            SwapBuffers();
        }

        float[] colorToFloatArray(Color color)
        {
            return new float[]
            {
                color.R / 255.0f,
                color.G / 255.0f,
                color.B / 255.0f,
                color.A / 255.0f,
            };
        }

        private void drawPartsM()
        {
            if(parts == null)
            {
                return;
            }
            GL.Enable(EnableCap.Lighting);
            GL.Enable(EnableCap.Light0);

            GL.Enable(EnableCap.Blend);
            GL.Enable(EnableCap.CullFace);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);

            GL.Material(MaterialFace.Front, MaterialParameter.Diffuse, colorToFloatArray(PartsColor));

            if (DrawParts)
            {
                foreach (var part in parts)
                {
                    part.DrawMesh();
                }
            }

            GL.Material(MaterialFace.Front, MaterialParameter.Diffuse, colorToFloatArray(PartsEdgesColor));
            GL.LineWidth(1);
            if (DrawPartsEdges)
            {
                foreach (var part in parts)
                {
                    part.DrawPaths();
                }
            }

            GL.Disable(EnableCap.Lighting);
            GL.Disable(EnableCap.Light0);
        }

        private void drawHightlightPath()
        {
            GL.Disable(EnableCap.DepthTest);
            GL.LineWidth(3);
            if (highlightPath != null)
            {
                highlightPath.Draw();
            }
        }

        private void drawPathsM()
        {
            GL.LineWidth(1);
            if (paths != null)
            {
                foreach (var path in paths)
                {
                    path.Draw();
                }
            }
        }
    }
}
