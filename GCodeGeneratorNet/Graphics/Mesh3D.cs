﻿using GCodeGeneratorNet.Core.Geometry;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using GCodeGeneratorNet.Graphics.Vertices;

namespace GCodeGeneratorNet.Graphics
{
    public class Mesh3D
    {
        
        Vbo vbo = null;
        IEnumerable<VertexPositionNormal> points;

        public Mesh3D(IEnumerable<Triangle3D> triangles)
        {
            points = triangles.SelectMany(t =>
           {
               return new[] 
               {
                   new VertexPositionNormal(t.V1, t.Normal),
                   new VertexPositionNormal(t.V2, t.Normal),
                   new VertexPositionNormal(t.V3, t.Normal)
               };
           });
        }

        public void Draw()
        {
            if (vbo == null)
            {
                vbo = Vbo.LoadVBO(points.ToArray());
            }

            GL.EnableClientState(ArrayCap.VertexArray);
            GL.EnableClientState(ArrayCap.NormalArray);

            GL.BindBuffer(BufferTarget.ArrayBuffer, vbo.VboID);

            var pc = new VertexPositionNormal[] { };
            var stride = BlittableValueType.StrideOf(pc);
            GL.VertexPointer(3, VertexPointerType.Float, stride, new IntPtr(0));
            GL.NormalPointer( NormalPointerType.Float, stride, new IntPtr(12));
            GL.DrawArrays(BeginMode.Triangles, 0, vbo.NumElements);
        }

        public void Dispose()
        {
            if (vbo != null)
            {
                GL.DeleteBuffers(1, ref vbo.VboID);
                vbo = null;
            }
        }
    }
}
