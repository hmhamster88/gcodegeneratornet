﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using GCodeGeneratorNet.Graphics.Vertices;

namespace GCodeGeneratorNet.Graphics
{
    public class Path3D
    {
        Vbo vbo = null;
        IEnumerable<VertexPosition> points;

        public IEnumerable<VertexPosition> Points
        {
            get
            {
                return points;
            }
        }

        public Path3D(IEnumerable<Vector3> points)
        {
            this.points = points.Select(p => new VertexPosition(p)).ToArray();
        }

        public Path3D(IEnumerable<VertexPosition> points)
        {
            this.points = points;
        }

        public void Draw()
        {
            if (vbo == null)
            {
                vbo = Vbo.LoadVBO(points.ToArray());
            }
            GL.EnableClientState(ArrayCap.VertexArray);

            GL.BindBuffer(BufferTarget.ArrayBuffer, vbo.VboID);

            VertexPosition[] pc = new VertexPosition[] { };
            GL.VertexPointer(3, VertexPointerType.Float, BlittableValueType.StrideOf(pc), new IntPtr(0));
            GL.DrawArrays(BeginMode.LineStrip, 0, vbo.NumElements);
        }

        public void Dispose()
        {
            if (vbo != null)
            {
                GL.DeleteBuffers(1, ref vbo.VboID);
                vbo = null;
            }
        }
    }
}
