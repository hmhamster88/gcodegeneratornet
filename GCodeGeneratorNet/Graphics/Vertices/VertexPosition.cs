﻿using System.Drawing;
using System.Runtime.InteropServices;
using OpenTK;

namespace GCodeGeneratorNet.Graphics.Vertices
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct VertexPosition
    {
        public Vector3 Position;

        public VertexPosition(float x, float y, float z)
        {
            Position = new Vector3(x, y, z);
        }

        public VertexPosition(Vector3 position)
        {
            Position = position;
        }

        public static uint ToRgba(Color color)
        {
            return (uint)color.A << 24 | (uint)color.B << 16 | (uint)color.G << 8 | (uint)color.R;
        }
    }
}