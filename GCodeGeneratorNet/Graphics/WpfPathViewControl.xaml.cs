﻿using GCodeGeneratorNet.Core;
using GCodeGeneratorNet.Core.GCodes;
using GCodeGeneratorNet.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GCodeGeneratorNet.Graphics
{
    /// <summary>
    /// Логика взаимодействия для WpfPathViewControl.xaml
    /// </summary>
    public partial class WpfPathViewControl : UserControl
    {
        public Color BackgroundColor
        {
            get { return (Color)GetValue(BackgroundColorProperty); }
            set { SetValue(BackgroundColorProperty, value); }
        }

        // Using a DependencyProperty as the backing store for BackgroundColor.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty BackgroundColorProperty =
            DependencyProperty.Register("BackgroundColor", typeof(Color), typeof(WpfPathViewControl), 
                new FrameworkPropertyMetadata(Colors.White, new PropertyChangedCallback(OnBackgroundColorChanged)));

        private static void OnBackgroundColorChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((WpfPathViewControl)d).BackgroundColorChanged((Color)e.NewValue);
        }

        private void BackgroundColorChanged(Color newValue)
        {
            pathView.BackgroundColor = newValue;
        }

        public Color PartsEdgesColor
        {
            get { return (Color)GetValue(PartsEdgesColorProperty); }
            set { SetValue(PartsEdgesColorProperty, value); }
        }

        // Using a DependencyProperty as the backing store for PartsEdgesColor.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PartsEdgesColorProperty =
            DependencyProperty.Register("PartsEdgesColor", typeof(Color), typeof(WpfPathViewControl),
                new FrameworkPropertyMetadata(Colors.Black, new PropertyChangedCallback(OnPartsEdgesColorChanged)));

        private static void OnPartsEdgesColorChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((WpfPathViewControl)d).PartsEdgesColorChanged((Color)e.NewValue);
        }

        private void PartsEdgesColorChanged(Color newValue)
        {
            pathView.PartsEdgesColor = newValue;
        }

        public Color PartsColor
        {
            get { return (Color)GetValue(PartsColorProperty); }
            set { SetValue(PartsColorProperty, value); }
        }

        // Using a DependencyProperty as the backing store for PartsColor.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PartsColorProperty =
            DependencyProperty.Register("PartsColor", typeof(Color), typeof(WpfPathViewControl),
                new FrameworkPropertyMetadata(Color.FromArgb(127, 127, 127, 127), new PropertyChangedCallback(OnPartsColorChanged)));

        private static void OnPartsColorChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((WpfPathViewControl)d).PartsColorChanged((Color)e.NewValue);
        }

        private void PartsColorChanged(Color newValue)
        {
            pathView.PartsColor = newValue;
        }

        public bool DrawPartsEdges
        {
            get { return (bool)GetValue(DrawPartsEdgesProperty); }
            set { SetValue(DrawPartsEdgesProperty, value); }
        }

        // Using a DependencyProperty as the backing store for DrawPartsEdges.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DrawPartsEdgesProperty =
            DependencyProperty.Register("DrawPartsEdges", typeof(bool), typeof(WpfPathViewControl), new FrameworkPropertyMetadata(true, new PropertyChangedCallback(OnDrawPartsEdgesChanged)));

        private static void OnDrawPartsEdgesChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((WpfPathViewControl)d).DrawPartsEdgesChanged((bool)e.NewValue);
        }

        private void DrawPartsEdgesChanged(bool newValue)
        {
            pathView.DrawPartsEdges = newValue;
        }

        public bool DrawPaths
        {
            get { return (bool)GetValue(DrawPathsProperty); }
            set { SetValue(DrawPathsProperty, value); }
        }

        // Using a DependencyProperty as the backing store for DrawPaths.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DrawPathsProperty =
            DependencyProperty.Register("DrawPaths", typeof(bool), typeof(WpfPathViewControl), 
                new FrameworkPropertyMetadata(true, new PropertyChangedCallback(OnDrawPathsChanged)));

        private static void OnDrawPathsChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((WpfPathViewControl)d).DrawPathsChanged((bool)e.NewValue);
        }

        private void DrawPathsChanged(bool newValue)
        {
            pathView.DrawPaths = newValue;
        }

        public bool DrawParts
        {
            get { return (bool)GetValue(DrawPartsProperty); }
            set { SetValue(DrawPartsProperty, value); }
        }

        public static readonly DependencyProperty DrawPartsProperty =
            DependencyProperty.Register("DrawParts", typeof(bool), typeof(WpfPathViewControl), 
                new FrameworkPropertyMetadata(true, new PropertyChangedCallback(OnDrawPartsChanged)));

        private static void OnDrawPartsChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((WpfPathViewControl)d).DrawPartsChanged((bool)e.NewValue);
        }

        private void DrawPartsChanged(bool newValue)
        {
            pathView.DrawParts = newValue;
        }

        public IGCode SelectedCode
        {
            get
            {
                return (IGCode)GetValue(SelectedCodeProperty);
            }
            set
            {
                SetValue(SelectedCodeProperty, value);
            }
        }

        public static readonly DependencyProperty SelectedCodeProperty = DependencyProperty.Register("SelectedCode",
            typeof(IGCode), typeof(WpfPathViewControl), new FrameworkPropertyMetadata(null, new PropertyChangedCallback(OnSelectedCodeChanged)));

        private static void OnSelectedCodeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((WpfPathViewControl)d).selectedCodeChanged();
        }

        private void selectedCodeChanged()
        {
            if(SelectedCode != null && visualResult != null && visualResult.GCodePathsDictionary.ContainsKey(SelectedCode))
            {

                pathView.HighlightPath = new Path3DColored(visualResult.GCodePathsDictionary[SelectedCode], System.Drawing.Color.Magenta);
            }
        }

        public IGCodeResult Result
        {
            get
            {
                return (IGCodeResult)GetValue(ResultProperty);
            }
            set
            {
                SetValue(ResultProperty, value);
            }
        }

        public static readonly DependencyProperty ResultProperty = DependencyProperty.Register("Result", 
            typeof(IGCodeResult), typeof(WpfPathViewControl), new FrameworkPropertyMetadata(null, new PropertyChangedCallback(OnResultChanged)));

        private static void OnResultChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((WpfPathViewControl)d).resultChanged();
        }

        GCodeVisualResult visualResult;

        private void resultChanged()
        {
            if (Result != null)
            {
                visualResult = new GCodeVisualResult(Result);
                pathView.LoadPoints(visualResult.AllPaths);
                pathView.Parts = Result.Parts.Select(p => new Part3DModel(p)).ToArray();
                //pathView.LoadMeshes(visualResult.Parts.Select(p => new Mesh3D(p.ToTrangles())).ToArray());
            }
        }

        private void settingsChanged()
        {

        }

        PathViewControl pathView;
        public WpfPathViewControl()
        {
            InitializeComponent();
            glHost.Child = pathView = new PathViewControl();
        }
    }
}
