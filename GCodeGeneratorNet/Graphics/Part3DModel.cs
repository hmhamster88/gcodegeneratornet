﻿using GCodeGeneratorNet.Core.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCodeGeneratorNet.Graphics
{
    public class Part3DModel: IDisposable
    {
        Mesh3D mesh;

        IEnumerable<Path3D> paths;

        public Part3DModel(Part25D part)
        {
            mesh = new Mesh3D(part.ToTrangles());
            paths = part.ToPaths().Select(path => new Path3D(path.Points)).ToArray();
        }

        public void Dispose()
        {
            if(mesh != null)
            {
                mesh.Dispose();
            }
            foreach (var path in paths)
            {
                path.Dispose();
            }
        }

        public void DrawMesh()
        {
            mesh.Draw();
        }

        public void DrawPaths()
        {
            foreach(var path in paths)
            {
                path.Draw();
            }
        }
    }
}
