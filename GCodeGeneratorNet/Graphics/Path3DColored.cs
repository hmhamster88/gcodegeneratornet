﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using GCodeGeneratorNet.Graphics.Vertices;

namespace GCodeGeneratorNet.Graphics
{
    public class Path3DColored: IDisposable
    {
        Vbo vbo = null;
        IEnumerable<VertexPositionColor> points;

        public IEnumerable<VertexPositionColor> Points
        {
            get
            {
                return points;
            }
        }

        public Path3DColored(IEnumerable<Vector3> points, System.Drawing.Color color)
        {
            this.points = points.Select(p => new VertexPositionColor(p, color));
        }

        public Path3DColored(IEnumerable<VertexPositionColor> points)
        {
            this.points = points;
        }

        public void Draw()
        {
            if (vbo == null)
            {
                vbo = Vbo.LoadVBO(points.ToArray());
            }
            GL.EnableClientState(ArrayCap.ColorArray);
            GL.EnableClientState(ArrayCap.VertexArray);

            GL.BindBuffer(BufferTarget.ArrayBuffer, vbo.VboID);

            VertexPositionColor[] pc = new VertexPositionColor[] { };
            GL.VertexPointer(3, VertexPointerType.Float, BlittableValueType.StrideOf(pc), new IntPtr(0));
            GL.ColorPointer(4, ColorPointerType.UnsignedByte, BlittableValueType.StrideOf(pc), new IntPtr(12));
            GL.DrawArrays(BeginMode.LineStrip, 0, vbo.NumElements);
        }

        public void Dispose()
        {
            if (vbo != null)
            {
                GL.DeleteBuffers(1, ref vbo.VboID);
                vbo = null;
            }
        }
    }
}
