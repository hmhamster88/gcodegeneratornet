﻿using GCodeGeneratorNet.Core;
using GCodeGeneratorNet.Core.GCodes;
using GCodeGeneratorNet.Core.Geometry;
using GCodeGeneratorNet.Graphics;
using GCodeGeneratorNet.Graphics.Vertices;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCodeGeneratorNet.Graphics
{
    public class GCodeVisualResult: IGCodeResult, IDisposable
    {
        public IEnumerable<Part25D> Parts { get; private set; }
        public IEnumerable<IGCode> Codes { get; private set; }

        public Path3DColored GCodesPath { get; private set; }
        public IEnumerable<Path3DColored> PartsPaths { get; private set; }
        public IEnumerable<Path3DColored> AllPaths
        {
            get
            {
                return new[] { GCodesPath };
            }
        }
        
        public Dictionary<IGCode, IEnumerable<Vector3>> GCodePathsDictionary { get; private set; }

        public GCodeVisualResult(IGCodeResult result)
        {
            Parts = result.Parts.ToArray();
            Codes = result.Codes.ToArray();
            generatePaths();
        }

        IEnumerable<VertexPositionColor> ConvertGCodes()
        {
            GCodePathsDictionary = new Dictionary<IGCode, IEnumerable<Vector3>>();
            bool absolute = true;
            Vector3 position = new Vector3();
            float maxRpm = 0;

            float rpm = 0;
            var spindleCodes = Codes.OfType<GSpindle>();
            if (spindleCodes.Count() > 0)
            {
                maxRpm = spindleCodes.Max(c => c.S);
            }
            else
            {
                maxRpm = rpm = 1;
            }

            foreach (var code in Codes)
            {
                var points = code.GetPoints(position, absolute);
                GCodePathsDictionary[code] = points;
                var spindle = code as GSpindle;
                if (spindle != null)
                {
                    rpm = spindle.S;
                }
                System.Drawing.Color color;
                if(code.Type == Core.GCodes.Type.G && code.Code == 0 )
                {
                    color = System.Drawing.Color.Green;
                }
                else
                {
                    var c = (int)(rpm * 255 / maxRpm);
                    color = System.Drawing.Color.FromArgb(c, c, c);
                }

                foreach (var p in points)
                {
                    position = p;
                    yield return new VertexPositionColor(p, color);
                }
            }
        }

        void generatePaths()
        {
            if (Codes != null)
            {
                GCodesPath = new Path3DColored(ConvertGCodes().ToArray());
            }
        }

        public void Dispose()
        {
            if (GCodesPath != null)
            {
                GCodesPath.Dispose();
            }
            if(PartsPaths != null)
            {
                foreach(var path in PartsPaths)
                {
                    path.Dispose();
                }
            }
        }
    }
}
