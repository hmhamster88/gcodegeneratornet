﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GCodeGeneratorNet.Model;
using Microsoft.Practices.Unity;

namespace GCodeGeneratorNet.ModelView
{
    public class SettingsModelView : ISettingsModelView
    {
        [Dependency]
        public IView3DSettings View3DSettings { get; set; }
    }
}
