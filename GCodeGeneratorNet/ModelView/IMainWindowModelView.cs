﻿using GCodeGeneratorNet.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCodeGeneratorNet.ModelView
{
    public interface IMainWindowModelView
    {
        IScriptsManager ScriptsManager { get; set; }
        IScriptComplier ScriptCompiler { get; set; }
        IGCodeGenerationManger GenerationManger { get; set; }
        IGCodeSender GCodeSender { get; set; }
        IView3DSettings View3DSettings { get; set; }
    }
}
