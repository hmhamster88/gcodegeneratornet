﻿using GCodeGeneratorNet.Misc;
using GCodeGeneratorNet.Model;
using GCodeGeneratorNet.View;
using Microsoft.Practices.Unity;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace GCodeGeneratorNet.ModelView
{
    public class MainWindowModelView : IMainWindowModelView
    {
        const string gcodeExt = ".cnc";
        const string stlExt = ".stl";

        [Dependency]
        public IScriptsManager ScriptsManager { get; set; }

        [Dependency]
        public IScriptComplier ScriptCompiler { get; set; }

        [Dependency]
        public IGCodeGenerationManger GenerationManger { get; set; }

        [Dependency]
        public IGCodeSender GCodeSender { get; set; }

        [Dependency]
        public IView3DSettings View3DSettings { get; set; }

        [Dependency]
        public IErrorReporter ErrorReporter { get; set; }

        public ICommand NewScript { get; private set; }

        public ICommand OpenScript { get; private set; }

        public ICommand SaveScript { get; private set; }

        public ICommand SaveScriptAs { get; private set; }

        public ICommand Compile { get; private set; }

        public ICommand Generate { get; private set; }

        public ICommand ExportGCode { get; private set; }

        public ICommand ExportSTL { get; private set; }

        public ICommand Settings { get; private set; }

        public ICommand WindowLoaded { get; private set; }

        public ICommand ResetParameters { get; private set; }

        public MainWindowModelView()
        {
            NewScript = new RelayCommand((_) =>
            {
                ScriptsManager.New();
            });

            OpenScript = new RelayCommand((_) =>
            {
                var dialog = new OpenFileDialog();
                dialog.Filter = "C# script (*.csc)|*.csc";
                if (dialog.ShowDialog() == true)
                {
                    ScriptsManager.Open(dialog.FileName);
                    Task.Run(() =>
                    {
                        generate();
                    });
                }
            });

            SaveScript = new RelayCommand((_) =>
            {
                if (ScriptsManager.Script.FilePath == null)
                {
                    var dialog = new SaveFileDialog();
                    dialog.Filter = "C# script (*.csc)|*.csc";
                    if (dialog.ShowDialog() == true)
                    {
                        ScriptsManager.Save(dialog.FileName);
                    }
                }
                else
                {
                    ScriptsManager.Save(ScriptsManager.Script.FilePath);
                }
            });

            SaveScriptAs = new RelayCommand((_) =>
            {
                var dialog = new SaveFileDialog();
                dialog.Filter = "C# script (*.csc)|*.csc";
                if (dialog.ShowDialog() == true)
                {
                    ScriptsManager.Save(dialog.FileName);
                }
            });

            Compile = new RelayCommand((_) =>
            {
                Task.Run(() =>
                {
                    compile();
                });
            });

            Generate = new RelayCommand((_) =>
            {
                Task.Run(() =>
                {
                    generate();
                });
            });

            ExportGCode = new RelayCommand((_) =>
            {
                if (ScriptsManager.Script.Result == null)
                {
                    generate();
                }
                var dialog = new SaveFileDialog();
                dialog.Filter = $"GCode (*{gcodeExt})|*{gcodeExt}";
                if (dialog.ShowDialog() == true)
                {
                    ScriptsManager.ExportGCode(dialog.FileName);
                }
            });

            ExportSTL = new RelayCommand((_) =>
            {
                if (ScriptsManager.Script.Result == null)
                {
                    generate();
                }
                generate();
                var dialog = new SaveFileDialog();
                dialog.Filter = $"STL (*{stlExt})|*{stlExt}";
                if (dialog.ShowDialog() == true)
                {
                    ScriptsManager.ExportSTL(dialog.FileName);
                }
            });

            Settings = new RelayCommand((_) =>
            {
                var settingsWindow = new SettingsWindow();
                settingsWindow.ShowDialog();
            });

            WindowLoaded = new RelayCommand(windowLoaded);

            ResetParameters = new RelayCommand((_) =>
            {
                try
                {
                    ScriptsManager.Script.ResetParameters();
                    Task.Run(() =>
                    {
                        compile();
                    });
                }
                catch (Exception ex)
                {
                    ErrorReporter.Report(ex);
                }
            });
        }

        private void windowLoaded(object param)
        {
            try
            {
                var app = (App)System.Windows.Application.Current;
                var path = string.IsNullOrEmpty(app.OpenFileName) ? Properties.Settings.Default.LastFile : app.OpenFileName;
                if (string.IsNullOrEmpty(path))
                {
                    path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "GCodeGeneratorNet", "Samples", "ChaosStar.csc");
                }
                if (File.Exists(path))
                {
                    ScriptsManager.Open(path);
                    Task.Run(() =>
                    {
                        generate();
                    });
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.ToString());
            }
        }

        private void generate()
        {
            try
            {
                if (ScriptsManager.Script.Generator == null)
                {
                    compile();
                }
                ScriptsManager.Script.Result = GenerationManger.Generate(ScriptsManager.Script.Generator);
                if (ScriptsManager.Script.FilePath != null)
                {
                    ScriptsManager.ExportGCode(ScriptsManager.Script.FilePath + gcodeExt);
                }
            }
            catch(Exception ex)
            {
                ErrorReporter.Report(ex);
            }
        }

        private void compile()
        {
            try
            {
                ScriptsManager.Script.Generator = ScriptCompiler.Compile(ScriptsManager.Script);
            }
            catch (Exception ex)
            {
                ErrorReporter.Report(ex);
            }
        }
    }
}
