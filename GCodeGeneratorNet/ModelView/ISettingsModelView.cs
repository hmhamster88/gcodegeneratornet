﻿using GCodeGeneratorNet.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCodeGeneratorNet.ModelView
{
    public interface ISettingsModelView
    {
        IView3DSettings View3DSettings { get; set; }
    }
}
