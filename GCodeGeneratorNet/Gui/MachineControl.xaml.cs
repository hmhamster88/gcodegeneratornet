﻿using GCodeGeneratorNet.Core;
using GCodeGeneratorNet.Core.GCodes;
using GCodeGeneratorNet.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GCodeGeneratorNet.Gui
{
    /// <summary>
    /// Логика взаимодействия для MachineControl.xaml
    /// </summary>
    public partial class MachineControl : UserControl
    {
        IGCodeSender Sender
        {
            get
            {
                return DataContext as IGCodeSender;
            }
        }

        public MachineControl()
        {
            InitializeComponent();
        }

        float moveStep
        {
            get
            {
                return (float)stepUpDown.Value;
            }
        }

        private void xPButton_Click(object sender, RoutedEventArgs e)
        {
            Sender.SendCodes(new IGCode[] { new G91(), new GMOVE(true, moveStep), new G90() });
        }

        private void xNButton_Click(object sender, RoutedEventArgs e)
        {
            Sender.SendCodes(new IGCode[] { new G91(), new GMOVE(true, -moveStep), new G90() });
        }

        private void yPButton_Click(object sender, RoutedEventArgs e)
        {
            Sender.SendCodes(new IGCode[] { new G91(), new GMOVE(true, 0, moveStep), new G90() });
        }

        private void yNButton_Click(object sender, RoutedEventArgs e)
        {
            Sender.SendCodes(new IGCode[] { new G91(), new GMOVE(true, 0, -moveStep), new G90() });
        }

        private void zPButton_Click(object sender, RoutedEventArgs e)
        {
            Sender.SendCodes(new IGCode[] { new G91(), new GMOVE(true, 0, 0, moveStep), new G90() });
        }

        private void zNButton_Click(object sender, RoutedEventArgs e)
        {
            Sender.SendCodes(new IGCode[] { new G91(), new GMOVE(true, 0, 0, -moveStep), new G90() });
        }

        private void resetButton_Click(object sender, RoutedEventArgs e)
        {
            Sender.Reset();
        }

        private void resetXButton_Click(object sender, RoutedEventArgs e)
        {
            Sender.SendCommand("G10 P0 L20 X0");
        }

        private void resetYButton_Click(object sender, RoutedEventArgs e)
        {
            Sender.SendCommand("G10 P0 L20 Y0");
        }

        private void resetZButton_Click(object sender, RoutedEventArgs e)
        {
            Sender.SendCommand("G10 P0 L20 Z0");
        }
    }
}
