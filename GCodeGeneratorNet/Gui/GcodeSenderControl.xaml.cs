﻿using GCodeGeneratorNet.Core;
using GCodeGeneratorNet.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GCodeGeneratorNet.Gui
{
    /// <summary>
    /// Логика взаимодействия для GcodeSenderControl.xaml
    /// </summary>
    public partial class GcodeSenderControl : UserControl
    {
        IGCodeSender Sender
        {
            get
            {
                return DataContext as IGCodeSender;
            }
        }

        public GcodeSenderControl()
        {
            InitializeComponent();
        }

        private void refreshPortsButton_Click(object sender, RoutedEventArgs e)
        {
            Sender.RefreshPorts();
        }

        private void sendAllCodesButton_Click(object sender, RoutedEventArgs e)
        {
            Sender.SendAllCodes();
        }

        private void commandTextbox_KeyUp(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Return)
            {
                Sender.SendCommand(commandTextbox.Text);
                commandTextbox.Text = string.Empty;
            }
        }

        private void connectButton_Click(object sender, RoutedEventArgs e)
        {
            if (Sender.IsConnected)
            {
                Sender.Disconnect();
            }
            else
            {
                Sender.Connect();
            }
        }
    }
}
