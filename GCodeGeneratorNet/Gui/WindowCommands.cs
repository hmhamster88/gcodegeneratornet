﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace GCodeGeneratorNet.Gui
{
    public class WindowCommands
    {
        static WindowCommands()
        {
            Compile = new RoutedCommand("Compile", typeof(MainWindow));
        }

        public static RoutedCommand Compile;
    }
}
