﻿using GCodeGeneratorNet.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCodeGeneratorNet.Core
{
    public class Workspace
    {
        public TextEditManager TextEditManager { get; private set; }
        public GCodeSender GCodeSender { get; private set; }
        public Workspace()
        {
            TextEditManager = new TextEditManager();
            GCodeSender = new GCodeSender();
        }
    }
}
