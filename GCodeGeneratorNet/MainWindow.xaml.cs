﻿using GCodeGeneratorNet.Core.Misc;
using System;
using System.CodeDom.Compiler;
using System.Windows;
using System.Windows.Controls;

namespace GCodeGeneratorNet
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            this.Closing += MainWindow_Closing;
            GDebug.WriteEvent += GDebug_debugEvent;
            GDebug.ClearEvent += GDebug_ClearEvent;
        }

        void GDebug_ClearEvent(object sender, EventArgs e)
        {
            debugView.Clear();
        }

        void GDebug_debugEvent(object sender, DebugEventHandlerArgs e)
        {
            Dispatcher.Invoke(() =>
            {
                debugView.AppendText(e.Message);
                debugView.CaretIndex = debugView.Text.Length;
                debugView.ScrollToEnd();
            });
        }

        void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Properties.Settings.Default.Save();
        }

        private void ListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selected = errorList.SelectedItem as CompilerError;
            if (selected != null)
            {
                sourceEditor.TextArea.Caret.Line = selected.Line;
                sourceEditor.TextArea.Caret.Column = selected.Column;
                sourceEditor.TextArea.Caret.BringCaretToView();
                sourceEditor.Focus();
            }
        }
    }
}
