﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCodeGeneratorNet.Core
{
    public class GCodeGeneratorHelperParameters
    {
        [DisplayName("Tolerance")]
        public float Tolerance { get; set; }

        [DisplayName("Tool Radius")]
        public float ToolRadius { get; set; }

        public float ToolRadiusAndTolerance { get { return Tolerance + ToolRadius; } }

        [DisplayName("Material Height")]
        public float MaterialHeight { get; set; }

        [DisplayName("Safety Height")]
        public float SafetyHeight { get; set; }

        [DisplayName("Rapid Move Rate")]
        public float RapidMoveRate { get; set; }

        [DisplayName("Horizontal Feed Rate")]
        public float HorizontalFeedRate { get; set; }

        [DisplayName("Vertical Feed Rate")]
        public float VerticalFeedRate { get; set; }

        [DisplayName("Vertical Step")]
        public float VerticalStep { get; set; }

        [DisplayName("Bridge Width")]
        public float BridgeWidth { get; set; }

        [DisplayName("Bridge Heidht")]
        public float BridgeHeidht { get; set; }

        [DisplayName("Bridge Count")]
        public int BridgeCount { get; set; }

        [DisplayName("Max RPM")]
        public float MaxRpm { get; set; }

        [DisplayName("Laser Mode")]
        public bool LaserMode { get; set; }

        [DisplayName("Laser Contour Repeat Count")]
        public int LaserContourRepeatCount { get; set; }

        [DisplayName("Part Contour Optimization")]
        public bool PartContourOptimization { get; set; }

        public GCodeGeneratorHelperParameters()
        { 
            MaterialHeight = 8;
            SafetyHeight = 4;
            Tolerance = 0;
            ToolRadius = 1;
            RapidMoveRate = 1000;
            HorizontalFeedRate = 200;
            VerticalFeedRate = 100;
            VerticalStep = 2;
            BridgeWidth = 2;
            BridgeHeidht = 0.5f;
            BridgeCount = 1;
            MaxRpm = 1000;
            LaserContourRepeatCount = 1;
            PartContourOptimization = true;
        
        }
    }
}
