﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GCodeGeneratorNet.Core.GCodes;
using GCodeGeneratorNet.Core.Geometry;

namespace GCodeGeneratorNet.Core
{
    public class GCodeResult : IGCodeResult
    {
        public IEnumerable<IGCode> Codes { get; private set; }

        public IEnumerable<Part25D> Parts { get; private set; }

        public GCodeResult(IEnumerable<Part25D> parts, IEnumerable<IGCode> codes)
        {
            Parts = parts;
            Codes = codes;
        }
    }
}
