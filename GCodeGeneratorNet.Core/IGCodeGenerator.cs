﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCodeGeneratorNet.Core
{
    public interface IGCodeGenerator
    {
        IGCodeGeneratorParameters Parameters { get; }
        IGCodeResult Result { get; }
    }
}
