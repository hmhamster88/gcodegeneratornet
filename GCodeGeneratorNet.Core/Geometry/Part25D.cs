﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TriangleNet.Geometry;

namespace GCodeGeneratorNet.Core.Geometry
{
    public class Part25D
    {
        public Contour Contour { get; private set; }
        public IEnumerable<Pocket> Pockets { get; private set; }
        public IEnumerable<Contour> Holes { get; private set; }
        public float Thickness { get; private set; }

        public Part25D(float thickness, Contour contour, IEnumerable<Pocket> pockets, IEnumerable<Contour> holes)
        {
            this.Thickness = thickness;
            this.Contour = contour;
            this.Pockets = pockets;
            this.Holes = holes;
        }

        IEnumerable<Vector3> ContourAt(Contour contour, float z)
        {
            return contour.DrawPoints.Select(v => new Vector3(v.X, v.Y, z));
        }

        public IEnumerable<Path3D> ToPaths()
        {
            if (Contour.Parts.Count() > 0)
            {
                yield return new Path3D(ContourAt(Contour, 0));
                yield return new Path3D(ContourAt(Contour, Thickness));
            }
            if (Holes != null)
            {
                foreach (var hole in Holes)
                {
                    yield return new Path3D(ContourAt(hole, 0));
                    yield return new Path3D(ContourAt(hole, Thickness));
                }
            }
            if (Pockets != null)
            {
                foreach (var pocket in Pockets)
                {
                    yield return new Path3D(ContourAt(pocket, Thickness - pocket.Depth));
                    yield return new Path3D(ContourAt(pocket, Thickness));
                }
            }
        }

        static IEnumerable<Triangle3D> Triangulation(Contour contour, IEnumerable<Contour> holes, float z, bool invertNormal = false)
        {
            var poly = new Polygon();
            var triangulated = contour.ToTriangulateContour();
            if (triangulated != null)
            {
                poly.Add(triangulated);
            }

            foreach (var hole in holes)
            {
                var converted = hole.ToTriangulateContour();
                if (converted != null)
                {
                    poly.Add(converted, true);
                }
            }

            TriangleNet.Meshing.IMesh mesh = null;

            try
            {
                mesh = poly.Triangulate();
            }
            catch { }

            var triangles = new List<Triangle3D>();

            if (mesh != null)
            {
                foreach (var triangle in mesh.Triangles)
                {
                    triangles.Add(Triangle3D.fromTriangle(triangle, z, invertNormal));
                }
            }

            return triangles;
        }

        public IEnumerable<Triangle3D> ToTrangles()
        {
            var result = new List<Triangle3D>();
            result.AddRange(Triangulation(Contour, Holes, 0, true));
            result.AddRange(Triangulation(Contour, Holes.Concat(Pockets), Thickness));
            result.AddRange(Triangle3D.VerticalContour(Contour.DrawPoints, 0, Thickness));
            if (Holes != null)
            {
                foreach (var hole in Holes)
                {
                    result.AddRange(Triangle3D.VerticalContour(hole.DrawPoints, 0, Thickness, true));
                }
            }
            if (Pockets != null)
            {
                foreach (var pocket in Pockets)
                {
                    result.AddRange(Triangle3D.VerticalContour(pocket.DrawPoints, Thickness - pocket.Depth, Thickness, true));
                    result.AddRange(Triangulation(pocket, Holes, Thickness - pocket.Depth));
                }
            }
            return result;
        }

        public Part25D Offset(Vector2 offset)
        {
            return new Part25D(Thickness, Contour.Offset(offset), Pockets.Select(p => (Pocket)p.Offset(offset)), Holes.Select(p => p.Offset(offset)));
        }

        public IEnumerable<Vector2> BoundingPoints
        {
            get
            {
                return Contour.BoundingPoints.Concat(Pockets.SelectMany(p => p.BoundingPoints)).Concat(Holes.SelectMany(h => h.BoundingPoints));
            }
        }

        public Rect BoundingBox
        {
            get
            {
                return Rect.BoundingBox(BoundingPoints);
            }
        }

        public Part25D Centered()
        {
            var rect = BoundingBox;
            return Offset(-rect.Position - rect.Size / 2);
        }

        public Part25D Positive()
        {
            var rect = BoundingBox;
            return Offset(-rect.Position);
        }
    }
}
