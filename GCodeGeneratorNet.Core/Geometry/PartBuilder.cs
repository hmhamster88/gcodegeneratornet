﻿using GCodeGeneratorNet.Core.Misc;
using IxMilia.Dxf;
using IxMilia.Dxf.Entities;
using OpenTK;
using Svg;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GCodeGeneratorNet.Core.Geometry
{
    public class PartBuilder
    {
        SimpleTextGenerator textGenerator = new SimpleTextGenerator();
        List<IContourPart> contourParts = new List<IContourPart>();
        Contour contour = new Contour(true);
        List<Contour> holes = new List<Contour>();
        List<Pocket> pockets = new List<Pocket>();

        public IEnumerable<Contour> AllContours
        {
            get
            {
                var result = new List<Contour>();
                result.Add(contour);
                result.AddRange(holes);
                result.AddRange(pockets);
                return result;
            }
        }

        public void SetContour(Contour contour)
        {
            this.contour = contour;
        }

        public void AddHole(Contour hole)
        {
            holes.Add(hole);
        }

        public void AddPocket(Pocket pocket)
        {
            pockets.Add(pocket);
        }

        public void Add(IContourPart part)
        {
            contourParts.Add(part);
        }

        public void AddPoint(Vector2 position)
        {
            Add(new ContourPoint(position));
        }

        public void AddArc(Vector2 center, float radius, Angle startAngle, Angle stopAngle, RotateDirection direction)
        {
            Add(new Arc(center, radius, startAngle, stopAngle, direction));
        }

        public void AddCircle(Vector2 center, float radius)
        {
            AddArc(center, radius, 0, 0, RotateDirection.CW);
        }

        public void AddRect(Vector2 position, Vector2 size)
        {
            AddPoint(position);
            AddPoint(position + new Vector2(size.X, 0));
            AddPoint(position + size);
            AddPoint(position + new Vector2(0, size.Y));
        }

        public void AddRoundRect(Vector2 size, float radius)
        {
            AddArc(new Vector2(radius, radius), radius, Angle.Down, Angle.Left, RotateDirection.CW);
            AddArc(new Vector2(radius, size.Y - radius), radius, Angle.Left, Angle.Up, RotateDirection.CW);
            AddArc(new Vector2(size.X - radius, size.Y - radius), radius, Angle.Up, Angle.Right, RotateDirection.CW);
            AddArc(new Vector2(size.X - radius, radius), radius, Angle.Right, Angle.Down, RotateDirection.CW);
        }

        public void AddRect(Rect rect)
        {
            AddRect(rect.Position, rect.Size);
        }

        public void AddRectFromCenter(Vector2 position, Vector2 size)
        {
            AddRect(position - size / 2, size);
        }

        public void AddSimpleText(string text, Vector2 offset, Angle rotation, bool centered)
        {
            var contours = textGenerator.CreateString(text, centered);
            foreach(var contour in contours)
            {
                AddHole(contour.Offset(offset).Rotate(offset, rotation));
            }
        }

        public IEnumerable<Contour> AddPath(GraphicsPath gp, Matrix matrix, float flatness, float? rpmOverride = null)
        {
            gp.Flatten(matrix, flatness);
            Vector2 lastPos = new Vector2();
            for (int i = 0; i < gp.PathPoints.Length; i++)
            {
                Vector2 v = new Vector2(gp.PathPoints[i].X, -gp.PathPoints[i].Y);
                if (gp.PathTypes[i] == 0)
                {
                    if (i != 0)
                    {
                        yield return CreateHole();
                    }
                    
                    lastPos = v;
                }
                AddPoint(v);
            }
            yield return CreateHole(rpmOverride: rpmOverride);
        }

        public void AddHorizontalFill(IEnumerable<Contour> contours, float step, float? rpmOverride = null)
        {
            if(step == 0)
            {
                return;
            }
            Rect rect = Contour.ListBoundingBox(contours);
            for (float y = rect.Position.Y; y < rect.RightBottom.Y; y += step)
            {
                var crossPoints = Contour.ListHorizontalCross(contours, y).OrderBy(v => v.X).ToArray();
                for (int i = 0; i < crossPoints.Count() - 1; i += 2)
                {
                    holes.Add(new Contour(new[] { new ContourPoint(crossPoints[i]), new ContourPoint(crossPoints[i + 1]) }, false, rpmOverride));
                }
            }
        }

        public IEnumerable<Contour> AddText(string text, FontFamily fontFamily, int style, float size, Matrix matrix, float flatness, bool centerd = true)
        {
            GraphicsPath gp = new GraphicsPath();
            gp.AddString(text, fontFamily, style, size, new System.Drawing.Point(), System.Drawing.StringFormat.GenericDefault);
            var m = new Matrix();
            var bounds = gp.GetBounds();
            if (centerd)
                m.Translate(-(bounds.X + bounds.Width / 2), -(bounds.Y + gp.GetBounds().Height / 2));
            gp.Transform(m);
            return AddPath(gp, matrix, flatness).ToArray();
        }

        public IEnumerable<Contour> AddText(string text, FontFamily fontFamily, int style, float size, Matrix matrix, RectangleF rect, float flatness, bool centerd = true, float? rpmOverride = null)
        {
            GraphicsPath gp = new GraphicsPath();
            gp.AddString(text, fontFamily, style, size, rect, System.Drawing.StringFormat.GenericDefault);
            var m = new Matrix();
            var bounds = gp.GetBounds();
            if (centerd)
                m.Translate(-(bounds.X + bounds.Width / 2), -(bounds.Y + gp.GetBounds().Height / 2));
            gp.Transform(m);
            return AddPath(gp, matrix, flatness).ToArray();
        }

        public Contour CreateContour()
        {
            contour = new Contour(contourParts, true);
            contourParts = new List<IContourPart>();
            return contour;
        }

        public Contour CreateHole(bool closed = true, float? rpmOverride = null)
        {
            if (contourParts.Count == 0)
            {
                return null;
            }
            var hole = new Contour(contourParts, closed, rpmOverride);
            holes.Add(hole);
            contourParts = new List<IContourPart>();
            return hole;
        }

        public void CreatePocket(float depth)
        {
            pockets.Add(new Pocket(depth, contourParts));
            contourParts = new List<IContourPart>();
        }

        public Part25D CreatePart(float thickness)
        {
            var part = new Part25D(thickness, contour, pockets, holes);
            contour = new Contour(true);
            pockets = new List<Pocket>();
            holes = new List<Contour>();
            return part;
        }

        public void ClearHoles()
        {
            holes.Clear();
        }

        public Rect BoundingBox
        {
            get
            {
                return Contour.ListBoundingBox(new[] { contour }.Concat(pockets).Concat(holes));
            }
        }

    }
}
