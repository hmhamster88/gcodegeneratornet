﻿using GCodeGeneratorNet.Core.Misc;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCodeGeneratorNet.Core.Geometry
{
    public class SimpleTextGenerator
    {
        Dictionary<char, IEnumerable<Contour>> chars = new Dictionary<char, IEnumerable<Contour>>();

        public SimpleTextGenerator()
        {
            chars['0'] = zero();
            chars['1'] = one();
            chars['2'] = two();
            chars['3'] = three();
            chars['4'] = four();
            chars['5'] = five();
            chars['6'] = six();
            chars['7'] = seven();
            chars['8'] = eight();
            chars['9'] = nine();
            chars['+'] = plus();
            chars['-'] = minus();
            chars['.'] = dot();
            chars['A'] = A();
            chars['B'] = B();
            chars['C'] = C();
            chars['D'] = D();
            chars['E'] = E();
            chars['F'] = F();
            chars['G'] = G();
            chars['H'] = H();
            chars['J'] = J();
            chars['I'] = I();
            chars['K'] = K();
            chars['L'] = L();
            chars['M'] = M();
            chars['N'] = N();
            chars['O'] = O();
            chars['P'] = P();
            chars['Q'] = Q();
            chars['R'] = R();
            chars['S'] = S();
            chars['T'] = T();
            chars['U'] = U();
            chars['V'] = V();
            chars['W'] = W();
            chars['X'] = X();
            chars['Y'] = Y();
            chars['Z'] = Z();
        }

        float space = 1;

        public IEnumerable<Contour> CreateString(string str, bool centered)
        {
            float shift = 0;
            float allWidth = width * str.Length + space * (str.Length - 1);
            foreach (char c in str)
            {
                IEnumerable<Contour> contours;
                if (!chars.TryGetValue(c, out contours))
                {
                    contours = empty();
                }
                foreach (var contour in contours)
                {
                    if (centered)
                    {
                        yield return contour.Offset(new Vector2(shift - allWidth / 2, -height / 2));
                    }
                    else
                    {
                        yield return contour.Offset(new Vector2(shift, 0));
                    }
                }
                shift += width + space;
            }
        }

        const float width = 4;
        const float height = 8;
        Vector2 charSize = new Vector2(width, height);
        Vector2 upCenter = new Vector2(width / 2, height * 3 / 4.0f);
        Vector2 downCenter = new Vector2(width / 2, height / 4);
        float radius = width / 2;


        IEnumerable<Contour> empty()
        {
            var builder = new ContourBuilder();
            builder.Add(0, 0);
            builder.Add(0, height);
            builder.Add(width, height);
            builder.Add(width, 0);
            yield return builder.ToContour(true);
        }

        IEnumerable<Contour> zero()
        {
            var builder = new ContourBuilder();
            builder.Add(upCenter, radius, Angle.Left, Angle.Right, RotateDirection.CW);
            builder.Add(downCenter, radius, Angle.Right, Angle.Left, RotateDirection.CW);
            yield return builder.ToContour(true);
            builder.Add(width / 4, height / 8);
            builder.Add(width * 3 / 4, height * 7 / 8);
        }

        IEnumerable<Contour> one()
        {
            var builder = new ContourBuilder();
            builder.Add(width / 4, 0);
            builder.Add(width * 3 / 4, 0);
            yield return builder.ToContour(false);
            builder.Add(width / 2, 0);
            builder.Add(width / 2, height);
            builder.Add(width / 4, height * 7 / 8);
            yield return builder.ToContour(false);
        }

        IEnumerable<Contour> two()
        {
            var builder = new ContourBuilder();
            builder.Add(upCenter, radius, Angle.Left, Angle.Right, RotateDirection.CW);
            builder.Add(0, 0);
            builder.Add(width, 0);
            yield return builder.ToContour(false);
        }

        IEnumerable<Contour> three()
        {
            var builder = new ContourBuilder();
            builder.Add(upCenter, radius, Angle.Left, Angle.Down, RotateDirection.CW);
            builder.Add(downCenter, radius, Angle.Up, Angle.Left, RotateDirection.CW);
            yield return builder.ToContour(false);
        }

        IEnumerable<Contour> four()
        {
            var builder = new ContourBuilder();
            builder.Add(0, height);
            builder.Add(upCenter, radius, Angle.Left, Angle.Down, RotateDirection.CCW);
            builder.Add(width, height / 2);
            yield return builder.ToContour(false);
            builder.Add(width, height);
            builder.Add(width, 0);
            yield return builder.ToContour(false);
        }

        IEnumerable<Contour> five()
        {
            var builder = new ContourBuilder();
            builder.Add(width, height);
            builder.Add(0, height);
            builder.Add(upCenter, radius, Angle.Left, Angle.Down, RotateDirection.CCW);
            builder.Add(downCenter, radius, Angle.Up, Angle.Left, RotateDirection.CW);
            yield return builder.ToContour(false);
        }

        IEnumerable<Contour> six()
        {
            var builder = new ContourBuilder();
            builder.Add(upCenter, radius, Angle.Right, Angle.Left, RotateDirection.CCW);
            builder.Add(downCenter, radius, Angle.Left, Angle.Left, RotateDirection.CCW);
            yield return builder.ToContour(false);
        }

        IEnumerable<Contour> seven()
        {
            var builder = new ContourBuilder();
            builder.Add(0, height);
            builder.Add(width, height);
            builder.Add(0, 0);
            yield return builder.ToContour(false);
        }

        IEnumerable<Contour> eight()
        {
            var builder = new ContourBuilder();
            builder.Add(upCenter, radius, Angle.Down, Angle.Down, RotateDirection.CCW);
            builder.Add(downCenter, radius, Angle.Up, Angle.Up, RotateDirection.CW);
            yield return builder.ToContour(false);
        }

        IEnumerable<Contour> nine()
        {
            var builder = new ContourBuilder();
            builder.Add(upCenter, radius, Angle.Right, Angle.Right, RotateDirection.CW);
            builder.Add(downCenter, radius, Angle.Right, Angle.Left, RotateDirection.CW);
            yield return builder.ToContour(false);
        }

        IEnumerable<Contour> plus()
        {
            var builder = new ContourBuilder();
            builder.Add(0, height / 2);
            builder.Add(width, height / 2);
            yield return builder.ToContour(false);
            builder.Add(width / 2, height / 2 + width / 2);
            builder.Add(width / 2, height / 2 - width / 2);
            yield return builder.ToContour(false);
        }

        IEnumerable<Contour> minus()
        {
            var builder = new ContourBuilder();
            builder.Add(0, height / 2);
            builder.Add(width, height / 2);
            yield return builder.ToContour(false);
        }

        IEnumerable<Contour> dot()
        {
            var builder = new ContourBuilder();
            builder.Add(width / 2, height / 16, height / 16, Angle.Up, Angle.Up, RotateDirection.CW);
            yield return builder.ToContour(false);
        }

        IEnumerable<Contour> A()
        {
            var builder = new ContourBuilder();
            builder.Add(0, 0);
            builder.Add(upCenter, radius, Angle.Left, Angle.Right, RotateDirection.CW);
            builder.Add(width, 0);
            yield return builder.ToContour(false);
            builder.Add(0, height / 2);
            builder.Add(width, height / 2);
            yield return builder.ToContour(false);
        }

        IEnumerable<Contour> B()
        {
            var builder = new ContourBuilder();
            builder.Add(0, height);
            builder.Add(upCenter, radius, Angle.Up, Angle.Down, RotateDirection.CW);
            builder.Add(0, height / 2);
            builder.Add(downCenter, radius, Angle.Up, Angle.Down, RotateDirection.CW);
            builder.Add(0, 0);
            yield return builder.ToContour(true);
        }

        IEnumerable<Contour> C()
        {
            var builder = new ContourBuilder();
            builder.Add(upCenter, radius, Angle.Right, Angle.Left, RotateDirection.CCW);
            builder.Add(downCenter, radius, Angle.Left, Angle.Right, RotateDirection.CCW);
            yield return builder.ToContour(false);
        }

        IEnumerable<Contour> D()
        {
            var builder = new ContourBuilder();
            builder.Add(0, height);
            builder.Add(upCenter, radius, Angle.Up, Angle.Right, RotateDirection.CW);
            builder.Add(downCenter, radius, Angle.Right, Angle.Down, RotateDirection.CW);
            builder.Add(0, 0);
            yield return builder.ToContour(true);
        }

        IEnumerable<Contour> E()
        {
            var builder = new ContourBuilder();
            builder.Add(width, height);
            builder.Add(0, height);
            builder.Add(0, 0);
            builder.Add(width, 0);
            yield return builder.ToContour(false);
            builder.Add(0, height / 2);
            builder.Add(width / 2, height / 2);
            yield return builder.ToContour(true);
        }

        IEnumerable<Contour> F()
        {
            var builder = new ContourBuilder();
            builder.Add(width, height);
            builder.Add(0, height);
            builder.Add(0, 0);
            yield return builder.ToContour(false);
            builder.Add(0, height / 2);
            builder.Add(width / 2, height / 2);
            yield return builder.ToContour(true);
        }

        IEnumerable<Contour> G()
        {
            var builder = new ContourBuilder();
            builder.Add(upCenter, radius, Angle.Right, Angle.Left, RotateDirection.CCW);
            builder.Add(downCenter, radius, Angle.Left, Angle.Right, RotateDirection.CCW);
            builder.Add(width / 2, height / 4);
            yield return builder.ToContour(false);
        }

        IEnumerable<Contour> H()
        {
            var builder = new ContourBuilder();
            builder.Add(0, height);
            builder.Add(0, 0);
            yield return builder.ToContour(false);
            builder.Add(0, height / 2);
            builder.Add(width, height / 2);
            yield return builder.ToContour(false);
            builder.Add(width, height);
            builder.Add(width, 0);
            yield return builder.ToContour(true);
        }

        IEnumerable<Contour> J()
        {
            var builder = new ContourBuilder();
            builder.Add(0, height);
            builder.Add(downCenter, radius, Angle.Left, Angle.Right, RotateDirection.CCW);
            yield return builder.ToContour(false);
        }

        IEnumerable<Contour> I()
        {
            var builder = new ContourBuilder();
            builder.Add(width / 4, 0);
            builder.Add(width * 3 / 4, 0);
            yield return builder.ToContour(false);
            builder.Add(width / 2, 0);
            builder.Add(width / 2, height);
            yield return builder.ToContour(false);
            builder.Add(width / 4, height);
            builder.Add(width * 3 / 4, height);
            yield return builder.ToContour(false);
        }

        IEnumerable<Contour> K()
        {
            var builder = new ContourBuilder();
            builder.Add(0, height);
            builder.Add(0, 0);
            yield return builder.ToContour(false);
            builder.Add(0, height / 2);
            builder.Add(width, height);
            yield return builder.ToContour(false);
            builder.Add(0, height / 2);
            builder.Add(width, 0);
            yield return builder.ToContour(false);
        }

        IEnumerable<Contour> L()
        {
            var builder = new ContourBuilder();
            builder.Add(0, height);
            builder.Add(0, 0);
            builder.Add(width, 0);
            yield return builder.ToContour(false);
        }

        IEnumerable<Contour> M()
        {
            var builder = new ContourBuilder();
            builder.Add(0, 0);
            builder.Add(0, height);
            builder.Add(width / 2, height - width / 2);
            builder.Add(width, height);
            builder.Add(width, 0);
            yield return builder.ToContour(false);
        }

        IEnumerable<Contour> N()
        {
            var builder = new ContourBuilder();
            builder.Add(0, 0);
            builder.Add(0, height);
            builder.Add(width, 0);
            builder.Add(width, height);
            yield return builder.ToContour(false);
        }

        IEnumerable<Contour> O()
        {
            var builder = new ContourBuilder();
            builder.Add(upCenter, radius, Angle.Left, Angle.Right, RotateDirection.CW);
            builder.Add(downCenter, radius, Angle.Right, Angle.Left, RotateDirection.CW);
            yield return builder.ToContour(true);
        }

        IEnumerable<Contour> P()
        {
            var builder = new ContourBuilder();
            builder.Add(0, height / 2);
            builder.Add(upCenter, radius, Angle.Down, Angle.Up, RotateDirection.CCW);
            builder.Add(0, height);
            builder.Add(0, 0);
            yield return builder.ToContour(false);
        }

        IEnumerable<Contour> Q()
        {
            var builder = new ContourBuilder();
            builder.Add(upCenter, radius, Angle.Left, Angle.Right, RotateDirection.CW);
            builder.Add(downCenter, radius, Angle.Right, Angle.Left, RotateDirection.CW);
            yield return builder.ToContour(true);
            builder.Add(width * 3 / 4, height / 8);
            builder.Add(width, 0);
            yield return builder.ToContour(false);
        }

        IEnumerable<Contour> R()
        {
            var builder = new ContourBuilder();
            builder.Add(width, 0);
            builder.Add(0, height / 2);
            builder.Add(upCenter, radius, Angle.Down, Angle.Up, RotateDirection.CCW);
            builder.Add(0, height);
            builder.Add(0, 0);
            yield return builder.ToContour(false);
        }

        IEnumerable<Contour> S()
        {
            var builder = new ContourBuilder();
            builder.Add(width, height);
            builder.Add(upCenter, radius, Angle.Up, Angle.Down, RotateDirection.CCW);
            builder.Add(downCenter, radius, Angle.Up, Angle.Down, RotateDirection.CW);
            builder.Add(0, 0);
            yield return builder.ToContour(false);
        }

        IEnumerable<Contour> T()
        {
            var builder = new ContourBuilder();
            builder.Add(width / 2, 0);
            builder.Add(width / 2, height);
            yield return builder.ToContour(false);
            builder.Add(0, height);
            builder.Add(width, height);
            yield return builder.ToContour(false);
        }

        IEnumerable<Contour> U()
        {
            var builder = new ContourBuilder();
            builder.Add(0, height);
            builder.Add(downCenter, radius, Angle.Left, Angle.Right, RotateDirection.CCW);
            builder.Add(width, height);
            yield return builder.ToContour(false);
        }

        IEnumerable<Contour> V()
        {
            var builder = new ContourBuilder();
            builder.Add(0, height);
            builder.Add(width / 2, 0);
            builder.Add(width, height);
            yield return builder.ToContour(false);
        }

        IEnumerable<Contour> W()
        {
            var builder = new ContourBuilder();
            builder.Add(0, height);
            builder.Add(0, 0);
            builder.Add(width / 2, width / 2);
            builder.Add(width, 0);
            builder.Add(width, height);
            yield return builder.ToContour(false);
        }

        IEnumerable<Contour> X()
        {
            var builder = new ContourBuilder();
            builder.Add(0, height);
            builder.Add(width, 0);
            yield return builder.ToContour(false);
            builder.Add(width, height);
            builder.Add(0, 0);
            yield return builder.ToContour(false);
        }

        IEnumerable<Contour> Y()
        {
            var builder = new ContourBuilder();
            builder.Add(0, height);
            builder.Add(width / 2, height / 2);
            builder.Add(width, height);
            yield return builder.ToContour(false);
            builder.Add(width / 2, height / 2);
            builder.Add(width / 2, 0);
            yield return builder.ToContour(false);
        }

        IEnumerable<Contour> Z()
        {
            var builder = new ContourBuilder();
            builder.Add(0, height);
            builder.Add(width, height);
            builder.Add(0, 0);
            builder.Add(width, 0);
            yield return builder.ToContour(false);
        }
    }
}
