﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriangleNet.Geometry
{
    public static class VertexExtension
    {
        public static Vector3 ToVector3(this Vertex v, float z)
        {
            return new Vector3((float)v.X, (float)v.Y, z);
        }
    }
}
