﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TriangleNet.Geometry;

namespace GCodeGeneratorNet.Core.Geometry
{
    public static class OpenTK
    {
        public static bool EqualEps(this Vector2 v1, Vector2 v2, float eps)
        {
            return (v1 - v2).Length < eps;
        }

        public static Vertex toVertex(this Vector2 v)
        {
            return new Vertex(v.X, v.Y);
        }
    }
}
