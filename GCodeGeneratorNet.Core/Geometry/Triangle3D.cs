﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TriangleNet;
using TriangleNet.Geometry;

namespace GCodeGeneratorNet.Core.Geometry
{
    public class Triangle3D
    {
        public Vector3 V1 { get; private set; }
        public Vector3 V2 { get; private set; }
        public Vector3 V3 { get; private set; }

        public Vector3 Normal { get; private set; }

        public Triangle3D(Vector3 v1, Vector3 v2, Vector3 v3, Vector3 normal = new Vector3())
        {
            V1 = v1;
            V2 = v2;
            V3 = v3;
            if(normal == Vector3.Zero)
            {
                var V = v2 - v1;
                var W = v3 - v1;
                normal = new Vector3(
                    V.Y * W.Z - V.Z * W.Y,
                    V.Z * W.X - V.X * W.Z,
                    V.X * W.Y - V.Y * W.X
                    );
                normal.Normalize();
            }
            Normal = normal;
        }

        public static IEnumerable<Triangle3D> VerticalPlane(Vector2 v1, Vector2 v2, float h1, float h2, bool invertNormal = false)
        {
            var v31 = new Vector3(v1.X, v1.Y, h1);
            var v32 = new Vector3(v2.X, v2.Y, h1);
            var v33 = new Vector3(v2.X, v2.Y, h2);
            var v34 = new Vector3(v1.X, v1.Y, h2);
            var d = (v2 - v1);
            var normal = new Vector3(d.Y, -d.X, 0);
            if (invertNormal)
            {
                return new[]
                {
                    new Triangle3D(v33, v32, v31, -normal),
                    new Triangle3D(v31, v34, v33, -normal),
                };
            }
            else
            {
                return new[]
                {
                    new Triangle3D(v31, v32, v33, normal),
                    new Triangle3D(v33, v34, v31, normal),
                };
            }
        }

        public static IEnumerable<Triangle3D> VerticalContour(IEnumerable<Vector2> points, float h1, float h2, bool invertNormal = false)
        {
            var result = new List<Triangle3D>();
            if(points.Count() == 0)
            {
                return result;
            }
            Vector2 prev = points.First();
            foreach (var curr in points)
            {
                if (curr != prev)
                {
                    result.AddRange(VerticalPlane(prev, curr, h1, h2, invertNormal));
                }
                prev = curr;
            }
            return result;
        }
        
        public static Triangle3D fromTriangle(TriangleNet.Topology.Triangle tri, float z, bool invertNormal = false)
        {
            if(invertNormal)
            {
                return new Triangle3D(tri.GetVertex(2).ToVector3(z), tri.GetVertex(1).ToVector3(z), tri.GetVertex(0).ToVector3(z),
                -Vector3.UnitZ);
            }
            return new Triangle3D(tri.GetVertex(0).ToVector3(z), tri.GetVertex(1).ToVector3(z), tri.GetVertex(2).ToVector3(z),
                Vector3.UnitZ);
        }
    }
}
