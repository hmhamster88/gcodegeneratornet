﻿using GCodeGeneratorNet.Core.GCodes;
using GCodeGeneratorNet.Core.Misc;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCodeGeneratorNet.Core.Geometry
{
    public enum ContourType
    {
        In,
        Out
    }
    public class Contour
    {
        public static Contour Rectangle(Vector2 position, Vector2 size)
        {
            return new Contour(true,
                new ContourPoint(position),
                new ContourPoint(position + new Vector2(size.X, 0)),
                new ContourPoint(position + size),
                new ContourPoint(position + new Vector2(0, size.Y))
                );
        }

        public bool Closed { get; private set; }

        public IEnumerable<IContourPart> Parts { get; private set; }

        public float? RpmOverride { get; set; }

        public Contour(IEnumerable<IContourPart> parts, bool closed = false, float? rpmOverride = null)
        {
            Parts = parts;
            Closed = closed;
            RpmOverride = rpmOverride;
        }

        public Contour(bool closed, params IContourPart[] parts)
        {
            Parts = parts;
            Closed = closed;
        }

        public Contour Inflate(float radius)
        {
            if(radius == 0)
            {
                return this;
            }
            if(Parts.Count() == 2)
            {
                if(Parts.First() is ContourPoint && Parts.Last() is ContourPoint)
                {
                    return this;
                }
            }
            var result = new List<IContourPart>();
            for (int i = 0; i < Parts.Count(); i++)
            {
                Vector2 prev;
                if (i == 0)
                {
                    prev = Parts.Last().LastPoint;
                }
                else
                {
                    prev = Parts.ElementAt(i - 1).LastPoint;
                }
                Vector2 next;
                if (i >= Parts.Count() - 1)
                {
                    next = Parts.First().FirstPoint;
                }
                else
                {
                    next = Parts.ElementAt(i + 1).FirstPoint;
                }

                result.AddRange(Parts.ElementAt(i).Inflate(radius, prev, next));
            }
            return new Contour(result);
        }

        public IEnumerable<Vector2> DrawPoints
        {
            get 
            {
                var points = Parts.SelectMany(p => p.DrawPoints);
                if (!Closed || Parts.Count() == 0)
                {
                    return points;
                }
                return points.Concat(new Vector2[] { Parts.First().DrawPoints.First() });
            }
        }

        public IEnumerable<IGCode> ToGCode(float f)
        {
            var codes = Parts.SelectMany(p => p.ToGCode(f));
            if (!Closed)
            {
                return codes;
            }
            var start = Parts.First().FirstPoint;
            return codes.Concat(new IGCode[]
            {
                new GMOVE(false, start.X, start.Y, null)
            });
        }

        public IEnumerable<IGCode> ToGCode(float z, float bridgeWidth, float bridgeHeight, int bridgeCount, float f)
        {
            var result = new List<IGCode>();
            var prev = Parts.Last().LastPoint;
            var start = Parts.First().FirstPoint;
            result.Add(new GMOVE(false, start.X, start.Y, null));
            result.Add(new GMOVE(false, null, null, z));
            foreach (var part in Parts)
            {
                result.AddRange(part.ToGCode(z, bridgeWidth, bridgeHeight, bridgeCount, f));
            }
            return result;
        }

        public IEnumerable<Vector2> BoundingPoints
        {
            get
            {
                return Parts.SelectMany(p => p.BoundingPoints);
            }
        }

        public Rect BoundingBox
        {
            get
            {
                return Rect.BoundingBox(BoundingPoints);
            }
        }

        public static Rect ListBoundingBox(IEnumerable<Contour> contours)
        {
            return Rect.BoundingBox(contours.SelectMany(c => c.BoundingPoints));
        }

        public Contour Offset(Vector2 offset)
        {
            return new Contour(Parts.Select(p => p.Offset(offset)), Closed, RpmOverride);
        }

        public Contour Rotate(Vector2 center, Angle angle)
        {
            return new Contour(Parts.Select(p => p.Rotate(center, angle)), Closed, RpmOverride);
        }

        public bool HorizontalLineCross(Vector2 v1, Vector2 v2, float y, ref Vector2 result)
        {
            if(v1.Y == v2.Y)
            {
                return false;
            }

            if (v1.Y >v2.Y)
            { 
                var tmp = v1;
                v1 = v2;
                v2 = tmp;

            }

            if(y < v1.Y || y > v2.Y)
            {
                return false;
            }

            float k = (v2.Y - v1.Y) / (v2.X - v1.X);
            float x = (y - v1.Y) / k + v1.X;

            result = new Vector2(x, y);
            return true;
        }

        public IEnumerable<Vector2> HorizontalCross(float y)
        {
            Vector2 vec = new Vector2();
            HorizontalLineCross(new Vector2(2, 1), new Vector2(4, 5), 6, ref vec);
            IContourPart prevPart = null;
            foreach(var part in Parts)
            {
                var partCross = part.HorizontalCross(y);
                if (partCross != null)
                {
                    foreach (var v in part.HorizontalCross(y))
                    {
                        yield return v;
                    }
                }
                if(prevPart == null && Closed)
                {
                    prevPart = Parts.Last();
                }
                if(prevPart != null)
                {
                    Vector2 result = new Vector2();
                    if (HorizontalLineCross(prevPart.LastPoint, part.FirstPoint, y, ref result))
                    {
                        yield return result;
                    }
                }
                prevPart = part;
            }
        }

        public static IEnumerable<Vector2> ListHorizontalCross(IEnumerable<Contour> contours, float y)
        {
            return contours.SelectMany(c => c.HorizontalCross(y));
        }

        public IEnumerable<Vector2> StartPoints
        {
            get
            {
                if (!Closed)
                {
                    yield return Parts.First().FirstPoint;
                    yield return Parts.Last().LastPoint;
                }
                else
                {
                    foreach(var part in Parts)
                    {
                        yield return part.FirstPoint;
                        if (part.FirstPoint != part.LastPoint)
                        {
                            yield return part.LastPoint;
                        }
                    }
                }
            }
        }

        public Vector2 NearestStartPoint(Vector2 v)
        {
            return StartPoints.MinBy(p => (p - v).Length);
        }

        public void Reverse()
        {
            Parts = Parts.Reverse().ToArray();
            foreach (var part in Parts)
            {
                part.Reverse();
            }
        }

        public TriangleNet.Geometry.Contour ToTriangulateContour()
        {
            var drawPoints = DrawPoints.ToArray();
            if (drawPoints.Length < 3)
            {
                return null;
            }
            return new TriangleNet.Geometry.Contour(DrawPoints.Select(p => p.toVertex()));
        }
    }
}
