﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCodeGeneratorNet.Core.Geometry
{
    public struct Rect
    {
        public Vector2 Position;
        public Vector2 Size;

        public Rect(Vector2 position, Vector2 size)
        {
            Position = position;
            Size = size;
        }

        public Vector2 RightBottom
        {
            get
            {
                return Position + Size;
            }
        }

        public Vector2 Center
        {
            get
            {
                return Position + Size / 2;
            }
        }

        public Rect Inflated(float f)
        {
            var v = new Vector2(f, f);
            return new Rect(Position - v, Size + v * 2);
        }

        public static Rect BoundingBox(IEnumerable<Vector2> boundingPoints)
        {
            if (boundingPoints.Count() == 0)
            {
                return new Rect();
            }

            Vector2 min;
            Vector2 max;
            min = max = boundingPoints.First();
            foreach (var p in boundingPoints)
            {
                if (p.X < min.X)
                {
                    min.X = p.X;
                }
                else if (p.X > max.X)
                {
                    max.X = p.X;
                }

                if (p.Y < min.Y)
                {
                    min.Y = p.Y;
                }
                else if (p.Y > max.Y)
                {
                    max.Y = p.Y;
                }
            }

            return new Rect(min, max - min);
        }

    }
}
