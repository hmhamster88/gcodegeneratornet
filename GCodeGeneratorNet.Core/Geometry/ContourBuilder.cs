﻿using GCodeGeneratorNet.Core.Misc;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCodeGeneratorNet.Core.Geometry
{
    public class ContourBuilder
    {
        List<IContourPart> parts = new List<IContourPart>();

        public void Add(float x, float y)
        {
            Add(new Vector2(x, y));
        }

        public void Add(Vector2 point)
        {
            parts.Add(new ContourPoint(point));
        }

        public void Add(float x, float y, float radius, Angle startAngle, Angle stopAngle, RotateDirection direction)
        {
            Add(new Vector2(x, y), radius, startAngle, stopAngle, direction);
        }

        public void Add(Vector2 point, float radius, Angle startAngle, Angle stopAngle, RotateDirection direction)
        {
            parts.Add(new Arc(point, radius, startAngle, stopAngle, direction));
        }

        public Contour ToContour(bool closed)
        {
            var array = parts.ToArray();
            parts.Clear();
            return new Contour(array, closed);
        }
    }
}
