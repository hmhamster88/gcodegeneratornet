﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCodeGeneratorNet.Core.Geometry
{
    public class Pocket: Contour
    {
        public float Depth { get; private set; }

        public Pocket(float depth, IEnumerable<IContourPart> parts) : base(parts, true)
        {
            Depth = depth;
        }

        public Pocket(float depth, params IContourPart[] parts): base(true, parts)
        {
            Depth = depth;
        }
    }
}
