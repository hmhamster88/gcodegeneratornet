﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCodeGeneratorNet.Core.Geometry
{
    public class Path3D
    {
        public IEnumerable<Vector3> Points { get; private set; }

        public Path3D(IEnumerable<Vector3> points)
        {
            Points = points;
        }
    }
}
