﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCodeGeneratorNet.Core
{
    public interface IFileHelper
    {
        Stream OpenRead(string filePath);
        IEnumerable<string> ReadLines(string filePath);
    }
}
