﻿using GCodeGeneratorNet.Core.GCodes;
using GCodeGeneratorNet.Core.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCodeGeneratorNet.Core
{
    public interface IGCodeResult
    {
        IEnumerable<IGCode> Codes { get; }
        IEnumerable<Part25D> Parts { get; }
    }
}
