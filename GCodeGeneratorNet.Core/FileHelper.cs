﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCodeGeneratorNet.Core
{
    public class FileHelper : IFileHelper
    {
        public string ScriptFilePath { get; private set; }
        public FileHelper(string scriptFilePath)
        {
            ScriptFilePath = scriptFilePath;
        }

        public string normalizeFileName(string filePath)
        {
            if (File.Exists(filePath))
            {
                return filePath;
            }
            return Path.Combine(Path.GetDirectoryName(ScriptFilePath), filePath);
        }

        public Stream OpenRead(string filePath)
        {
            return File.OpenRead(normalizeFileName(filePath));
        }

        public IEnumerable<string> ReadLines(string filePath)
        {
            return File.ReadAllLines(normalizeFileName(filePath));
        }
    }
}
