﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCodeGeneratorNet.Core
{
    public interface IFileHelperProvider
    {
        IFileHelper FileHelper { get; set; }
    }
}
